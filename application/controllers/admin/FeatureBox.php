<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class FeatureBox extends BaseController {


public function __construct()
{
    parent::__construct();
    $this->isLoggedIn();   
}

public function index()
{
   $pages = $this->common_model->getAllData('pages');

 $featureBoxData = $this->featureBox_model->featureboxFull();

  $data=['pageName'=>"FeatureBox",
         'featureBoxData' => $featureBoxData, 
          'options' => $pages,
          'action'  => 'add'
        ];

  $this->load->view('dashboard/featureBox',$data);

}

public function add()
{
    $data = array
    (
        'title' => $_POST['title'],
        'content' =>$_POST['content'],
        'button_text' => $_POST['buttonText'],
        'link' =>$_POST['link'],
        'pageId' => $_POST['page']
    );  
  
      $this->db->insert('feature_box', $data);

      $pages = $this->common_model->getAllData('pages');

      $featureBoxData = $this->featureBox_model->featureboxFull();

      $data=[   'pageName'=>"FeatureBox",
                'featureBoxData' => $featureBoxData, 
                'options' => $pages,
                'action'  => 'add',
                'check' => 'success'
            ];

         $this->load->view('dashboard/featureBox',$data);

}

public function delete($id){

  $this->common_model->delete('feature_box', $id);

  $pages = $this->common_model->getAllData('pages');
  $featureBoxData = $this->featureBox_model->featureboxFull();
  $data=[   'pageName'=>"FeatureBox",
            'featureBoxData' => $featureBoxData, 
            'options' => $pages,
            'action'  => 'add'
      ];
   $this->load->view('dashboard/featureBox',$data); 

}

public function loadUpdate($id){

  $updateData = $this->common_model->getById('feature_box',$id);
  $selectedPageData = $this->common_model->getById('pages',$updateData->pageId);
  $pages = $this->common_model->getAllData('pages');

  $featureBoxData = $this->featureBox_model->featureboxFull();
  

  $tableData=[  'pageName'=>"FeatureBox",
                'featureBoxData' => $featureBoxData, 
                'options' => $pages,
                'updateData'  => $updateData,
                'action'  => 'update',
                'selectedPage' => $selectedPageData
             ];

  $this->load->view('dashboard/featureBox', $tableData);

}

public function update($id){

  $data = array
          (
            'title' => $_POST['title'],
            'content' =>$_POST['content'],
            'button_text' => $_POST['buttonText'],
            'link' =>$_POST['link'],
            'pageId' => $_POST['page']
          ); 

    $this->common_model->update('feature_box', $id, $data);

    $featureBoxData = $this->featureBox_model->featureboxFull();
    $pages = $this->common_model->getAllData('pages');
  
    $tableData=['pageName'=>"FeatureBox",
                'featureBoxData' => $featureBoxData,
                'action'  => 'add',
                'options' => $pages
                ];
  
    $this->load->view('dashboard/featureBox', $tableData);

}


public function api(){

  $id = $_POST['id'];
  $data = $this->common_model->getById('feature_box',$id);
  $pages = $this->featureBox_model->getpage($id);

  $data->pages= $pages;
  
  $output = json_encode($data);
  echo $output;
}



} 

?>