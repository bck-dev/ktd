<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Reviews extends BaseController {


public function __construct()
{
    parent::__construct();
    $this->isLoggedIn();   
}

public function index()
{
  $reviewData = $this->common_model->getAllData('reviews');
  $locations = $this->common_model->getAllData('locations');
  $doctors = $this->common_model->getAllData('doctors');

  $data=['pageName'=>"Reviews",
         'reviewData' => $reviewData,
         'locations' => $locations,
         'doctors' => $doctors,
          'action'  => 'add'
        ];

  $this->load->view('dashboard/reviews',$data);

}

public function add()
{

  $data = array
  (
      'name' => $_POST['name'],
      'title' =>$_POST['title'],
      'rating' => $_POST['rating'],
      'review' =>$_POST['review'],
      'type' => $_POST['type'],
      'locationId' =>$_POST['location'],
      'doctorId' => $_POST['doctor']
  );  

    $this->db->insert('reviews', $data);

    $reviewData = $this->common_model->getAllData('reviews');
    $locations = $this->common_model->getAllData('locations');
    $doctors = $this->common_model->getAllData('doctors');
  
    $data=['pageName'=>"Reviews",
            'reviewData' => $reviewData,
            'locations' => $locations,
            'doctors' => $doctors,
            'action'  => 'add',
            'check' => 'success'
          ];
  
    $this->load->view('dashboard/reviews',$data);

}

public function delete($id){

  $this->common_model->delete('reviews', $id);

  $reviewData = $this->common_model->getAllData('reviews');
  $locations = $this->common_model->getAllData('locations');
  $doctors = $this->common_model->getAllData('doctors');

  $data=['pageName'=>"Reviews",
         'reviewData' => $reviewData,
         'locations' => $locations,
         'doctors' => $doctors,
          'action'  => 'add'
        ];

   $this->load->view('dashboard/reviews',$data); 

}

public function loadUpdate($id){

  $updateData = $this->review_model->reviewFull($id);
  $locations = $this->common_model->getAllData('locations');
  $doctors = $this->common_model->getAllData('doctors');

  $reviewData = $this->common_model->getAllData('reviews');

  $tableData=[  'pageName'=>"Reviews",
                'reviewData' => $reviewData,
                'updateData'  => $updateData,
                'locations' => $locations,
                'doctors' => $doctors,
                'action'  => 'update'
             ];

  $this->load->view('dashboard/reviews', $tableData);

}

public function update($id){

  $data = array
          (
            'name' => $_POST['name'],
            'title' =>$_POST['title'],
            'rating' => $_POST['rating'],
            'review' =>$_POST['review'],
            'type' => $_POST['type'],
            'locationId' =>$_POST['location'],
            'doctorId' => $_POST['doctor']
          ); 

    $this->common_model->update('reviews', $id, $data);

    $reviewData = $this->common_model->getAllData('reviews');

  
    $tableData=['pageName'=>"Reviews",
                'reviewData' => $reviewData,
                'action'  => 'add'
                ];
  
    $this->load->view('dashboard/reviews', $tableData);

}


public function api(){

  $id = $_POST['id'];
    
  $data = $this->review_model->reviewFull($id);
  
  $output = json_encode($data);
  echo $output;
}



} 

?>