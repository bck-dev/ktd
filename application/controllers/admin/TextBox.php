<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class TextBox extends BaseController {


public function __construct()
{
    parent::__construct();
    $this->isLoggedIn();   
}

public function index()
{
   $pages = $this->common_model->getAllData('pages');

   $textBoxData = $this->textBox_model->textboxFull();

  $data=[ 'pageName'=>"TextBox",
          'textBoxData' => $textBoxData, 
          'options' => $pages,
          'action'  => 'add'
        ];

  $this->load->view('dashboard/textBox',$data);

}

public function add()
{
    $data = array
    (
        'title' => $_POST['title'],
        'content' =>$_POST['content'],
        'link' =>$_POST['link'],
        'pageId' => $_POST['page']
    );  
  
      $this->db->insert('text_box', $data);

      $pages = $this->common_model->getAllData('pages');

      $textBoxData = $this->textBox_model->textboxFull();

      $data=[   'pageName'=>"TextBox",
                'textBoxData' => $textBoxData, 
                'options' => $pages,
                'action'  => 'add',
                'check' => 'success'
            ];

         $this->load->view('dashboard/textBox',$data);

}

public function delete($id){

  $this->common_model->delete('text_box', $id);

  $pages = $this->common_model->getAllData('pages');
  $textBoxData = $this->textBox_model->textboxFull();
  $data=[   'pageName'=>"TextBox",
            'textBoxData' => $textBoxData, 
            'options' => $pages,
            'action'  => 'add'
      ];
   $this->load->view('dashboard/textBox',$data); 

}

public function loadUpdate($id){

  $updateData = $this->common_model->getById('text_box',$id);
  $selectedPageData = $this->common_model->getById('pages',$updateData->pageId);
  $pages = $this->common_model->getAllData('pages');

  $textBoxData = $this->textBox_model->textboxFull();
  

  $tableData=[  'pageName'=>"TextBox",
                'textBoxData' => $textBoxData,  
                'options' => $pages,
                'updateData'  => $updateData,
                'action'  => 'update',
                'selectedPage' => $selectedPageData
             ];

  $this->load->view('dashboard/textBox', $tableData);

}

public function update($id){

  $data = array
          (
            'title' => $_POST['title'],
            'content' =>$_POST['content'],
            'link' =>$_POST['link'],
            'pageId' => $_POST['page']
          ); 

    $this->common_model->update('text_box', $id, $data);

    $textBoxData = $this->textBox_model->textboxFull();
    $pages = $this->common_model->getAllData('pages');
  
    $tableData=['pageName'=>"TextBox",
                'textBoxData' => $textBoxData,
                'action'  => 'add',
                'options' => $pages
                ];
  
    $this->load->view('dashboard/textBox', $tableData);

}

public function api(){

  $id = $_POST['id'];
  $data = $this->common_model->getById('text_box',$id);
  $pages = $this->textBox_model->getpage($id);

  $data->pages= $pages;
  
  $output = json_encode($data);
  echo $output;
}


} 

?>