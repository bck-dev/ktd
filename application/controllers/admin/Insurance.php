<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Insurance extends BaseController {


public function __construct()
{
    parent::__construct();
    $this->isLoggedIn();   
}

public function index()
{
    $insuranceData = $this->common_model->getAllData('insurance');
  
    $data=['pageName'=>"Insurance",
            'insuranceData' => $insuranceData, 
            'action'  => 'add',
          ];

     $this->load->view('dashboard/insurance',$data);

}

public function add()
{
    $data = array
    (
        'insuranceName' => $_POST['insuranceName'],
    );  
  
      $this->db->insert('insurance', $data);

      $insuranceData = $this->common_model->getAllData('insurance');

      $data=['pageName'=>"Insurance",
            'insuranceData' => $insuranceData,
            'check' => 'success',
            'action'  => 'add'
            ];

    $this->load->view('dashboard/insurance',$data);

}

public function delete($id){

  $this->common_model->delete('insurance', $id);
  $insuranceData = $this->common_model->getAllData('insurance');

  $data=['pageName'=>"Insurance",
          'insuranceData' => $insuranceData,
          'action'  => 'add'
      ];
  $this->load->view('dashboard/insurance', $data); 

}

public function loadUpdate($id){

  $insuranceData = $this->common_model->getAllData('insurance');
  $updateData = $this->common_model->getById('insurance',$id);

  $tableData=['pageName'=>"Insurance",
              'insuranceData' => $insuranceData ,
              'updateData' => $updateData,
              'action'  => 'update'
            ];

  $this->load->view('dashboard/insurance', $tableData);

}

public function update($id){
  
    $data = array
    (
            'insuranceName' => $_POST['insuranceName'],
    ); 

    $this->common_model->update('insurance', $id, $data);

    $insuranceData = $this->common_model->getAllData('insurance');
  
    $tableData=['pageName'=>"Insurance",
                'insuranceData' => $insuranceData ,
                'action'  => 'add',
                ];
  
    $this->load->view('dashboard/insurance', $tableData);

}

} 

?>