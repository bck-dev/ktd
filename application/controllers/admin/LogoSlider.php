<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class LogoSlider extends BaseController {


public function __construct()
{
    parent::__construct();
    $this->isLoggedIn();   
}

public function index()
{
   $pages = $this->common_model->getAllData('pages');

   $logoSliderData = $this->logoSlider_model->logoSliderFull();
  
  $data=[ 'pageName'=>"LogoSlider",
          'logoSliderData' => $logoSliderData, 
          'options' => $pages,
          'action'  => 'add'
         ];

  $this->load->view('dashboard/logoSlider',$data);

}

public function add()
{

    $config['upload_path']          = './uploads/';
    $config['allowed_types']        = 'jpg|png|jpeg';
    $config['max_size']             = 5000;
    $config['encrypt_name']         = TRUE;

    $this->load->library('upload', $config);

    $this->upload->do_upload('logoImage');
    
    $upload_data = $this->upload->data();
    $image_name = $upload_data['file_name'];

    $data = array
    (
        'logo_order' => $_POST['logo_order'],
        'image' => $image_name,
        'pageId' => $_POST['page']
    );  
  
      $this->db->insert('logo_slider', $data);

      $pages = $this->common_model->getAllData('pages');

      $logoSliderData = $this->logoSlider_model->logoSliderFull();

      $data=['pageName'=>"LogoSlider",
              'logoSliderData' => $logoSliderData, 
              'options' => $pages,
              'check' => 'success',
              'action'  => 'add'
             ];

    $this->load->view('dashboard/logoSlider',$data);

}

public function delete($id){

  $this->common_model->delete('logo_slider', $id);

  $pages = $this->common_model->getAllData('pages');
  $logoSliderData = $this->logoSlider_model->logoSliderFull();
  $data=[ 'pageName'=>"LogoSlider",
          'logoSliderData' => $logoSliderData,
          'options' => $pages,
          'action'  => 'add'
      ];
  $this->load->view('dashboard/logoSlider', $data); 

}

public function loadUpdate($id){

  $updateData = $this->common_model->getById('logo_slider',$id);
  $selectedPageData = $this->common_model->getById('pages',$updateData->pageId);
  
  $pages = $this->common_model->getAllData('pages');

  $logoSliderData = $this->logoSlider_model->logoSliderFull();

  $tableData=['pageName'=>"LogoSlider",
              'logoSliderData' => $logoSliderData,
              'updateData'  => $updateData,
              'action'  => 'update',
              'options' => $pages,
              'selectedPage' => $selectedPageData
              ];

  $this->load->view('dashboard/logoSlider', $tableData);

}

public function update($id){
  $config['upload_path']          = './uploads/';
  $config['allowed_types']        = 'jpg|png|jpeg';
  $config['max_size']             = 5000;
  $config['encrypt_name']         = TRUE;

  $this->load->library('upload', $config);
  $this->upload->do_upload('logoImage');
 
  $uploadData = $this->upload->data();
 
  if($uploadData['file_path']!=$uploadData['full_path']){
    
    $data = array
    (
      'logo_order' => $_POST['logo_order'],
      'image' =>$uploadData['file_name'],
      'pageId' => $_POST['page']
    );   
  }
  else{
    $data = array
    (
        'logo_order' => $_POST['logo_order'],
        'pageId' => $_POST['page']
    ); 
  }

  $this->common_model->update('logo_slider', $id, $data);
  $logoSliderData = $this->logoSlider_model->logoSliderFull();
  $pages = $this->common_model->getAllData('pages');

  $tableData=['pageName'=>"LogoSlider",
              'logoSliderData' => $logoSliderData ,
              'action'  => 'add',
              'options' => $pages
              ];

  $this->load->view('dashboard/logoSlider', $tableData);

}
} 

?>