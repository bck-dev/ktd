<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class HighlightedBox extends BaseController {


public function __construct()
{
    parent::__construct();
    $this->isLoggedIn();   
}

public function index()
{
   $pages = $this->common_model->getAllData('pages');

   $highlightedBoxData = $this->highlightedBox_model->highlightedBoxFull();
  
   $data=[   'pageName'=>"HighlightedBox",
              'highlightedBoxData' => $highlightedBoxData,
              'options' => $pages,
              'action'  => 'add'
          ];

  $this->load->view('dashboard/highlightedbox',$data);

}

public function add()
{

    $config['upload_path']          = './uploads/';
    $config['allowed_types']        = 'jpg|png';
    $config['max_size']             = 5000;
    $config['encrypt_name']         = TRUE;

    $this->load->library('upload', $config);

    $this->upload->do_upload('highlightedBoxImage');
    
    $upload_data = $this->upload->data();
    $image_name = $upload_data['file_name'];

    $data = array
    (
        'title' => $_POST['title'],
        'content' => $_POST['content'],
        'color' => $_POST['color'],
        'image' => $image_name,
        'pageId' => $_POST['page']
    );  

      $this->db->insert('highlighted_box', $data);

       $pages = $this->common_model->getAllData('pages');

       $highlightedBoxData = $this->highlightedBox_model->highlightedBoxFull();

      $data=['pageName'=>"HighlightedBox",
              'highlightedBoxData' => $highlightedBoxData, 
              'options' => $pages,
              'check' => 'success',
              'action'  => 'add'
    ];

      $this->load->view('dashboard/highlightedbox',$data);

}

public function delete($id){

  $this->common_model->delete('highlighted_box', $id);

  $pages = $this->common_model->getAllData('pages');
  $highlightedBoxData = $this->highlightedBox_model->highlightedBoxFull();
  $data=['pageName'=>"HighlightedBox",
          'highlightedBoxData' => $highlightedBoxData,
          'options' => $pages,
          'action'  => 'add'
      ];
  $this->load->view('dashboard/highlightedbox', $data); 

}

public function loadUpdate($id){

  $updateData = $this->common_model->getById('highlighted_box',$id);
  $selectedPageData = $this->common_model->getById('pages',$updateData->pageId);

  $pages = $this->common_model->getAllData('pages');

  $highlightedBoxData = $this->highlightedBox_model->highlightedBoxFull();
 
  $tableData=['pageName'=>"HighlightedBox",
              'highlightedBoxData' => $highlightedBoxData,
              'updateData'  => $updateData,
              'action'  => 'update',
              'options' => $pages,
              'selectedPage' => $selectedPageData
            ];

  $this->load->view('dashboard/highlightedbox', $tableData);

}

public function update($id){
  
  $config['upload_path']          = './uploads/';
  $config['allowed_types']        = 'jpg|png';
  $config['max_size']             = 5000;
  $config['encrypt_name']         = TRUE;

  $this->load->library('upload', $config);
  $this->upload->do_upload('highlightedBoxImage');
 
  $uploadData = $this->upload->data();
 
  if($uploadData['file_path']!=$uploadData['full_path']){
    
    $data = array
    (   
      'title' => $_POST['title'],
      'content' => $_POST['content'],
      'color' => $_POST['color'],
      'image' =>$uploadData['file_name'],
      'pageId' => $_POST['page']
    ); 

    
  }
  else{
    $data = array
    (
        'title' => $_POST['title'],
        'content' => $_POST['content'],
        'color' => $_POST['color'],
        'pageId' => $_POST['page']
    ); 
  }

  $this->common_model->update('highlighted_box', $id, $data);
  $highlightedBoxData = $this->highlightedBox_model->highlightedBoxFull();
  $pages = $this->common_model->getAllData('pages');

  $tableData=['pageName'=>"HighlightedBox",
              'highlightedBoxData' => $highlightedBoxData,
              'action'  => 'add',
              'options' => $pages
              ];

  $this->load->view('dashboard/highlightedbox', $tableData);

}

public function api(){

  $id = $_POST['id'];
  $data = $this->common_model->getById('highlighted_box',$id);
  $pages = $this->highlightedBox_model->getpage($id);

  $data->pages= $pages;
  
  $output = json_encode($data);
  echo $output;
}


} 

?>