<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class InsuranceLocation extends BaseController {

public function __construct()
{
    parent::__construct();
    $this->isLoggedIn();   
}

public function index()
{

    $locations = $this->common_model->getAllData('locations');
    $insurances = $this->common_model->getAllData('insurance');

    $insuranceData = $this->insurance_model->insuranceFull();
    $locationData = $this->insurance_model->locationFull();
   
    $data=[   'pageName'=>"InsuranceLocation",
               'locations' => $locations,
               'insurances' => $insurances,
               'insuranceData' => $insuranceData,
               'locationData' => $locationData,
               'action'  => 'add'
           ];

    $this->load->view('dashboard/insurancelocation',$data);

}

public function add()
{

    $data = array
    (
        'locationId' => $_POST['locationId'],
        'insuranceId' => $_POST['insuranceId']
    );  

      $this->db->insert('location_has_insurance', $data);

      $locations = $this->common_model->getAllData('locations');
      $insurances = $this->common_model->getAllData('insurance');

    //    $highlightedBoxData = $this->highlightedbox_model->highlightedBoxFull();

      $data=['pageName'=>"InsuranceLocation",
              //'highlightedBoxData' => $highlightedBoxData, 
              'locations' => $locations,
              'insurances' => $insurances,
              'check' => 'success',
              'action'  => 'add'
    ];

      $this->load->view('dashboard/insurancelocation',$data);

}

public function delete($id){

  $this->common_model->delete('highlighted_box', $id);

  $pages = $this->common_model->getAllData('pages');
  $highlightedBoxData = $this->highlightedbox_model->highlightedBoxFull();
  $data=['pageName'=>"HighlightedBox",
          'highlightedBoxData' => $highlightedBoxData,
          'options' => $pages,
          'action'  => 'add'
      ];
  $this->load->view('dashboard/highlightedbox', $data); 

}

public function loadUpdate($id){

  $updateData = $this->common_model->getById('highlighted_box',$id);
  $selectedPageData = $this->common_model->getById('pages',$updateData->pageId);

  $pages = $this->common_model->getAllData('pages');

  $highlightedBoxData = $this->highlightedbox_model->highlightedBoxFull();
 
  $tableData=['pageName'=>"HighlightedBox",
              'highlightedBoxData' => $highlightedBoxData,
              'updateData'  => $updateData,
              'action'  => 'update',
              'options' => $pages,
              'selectedPage' => $selectedPageData
            ];

  $this->load->view('dashboard/highlightedbox', $tableData);

}

public function update($id){
  
  $config['upload_path']          = './uploads/';
  $config['allowed_types']        = 'jpg|png';
  $config['max_size']             = 5000;
  $config['encrypt_name']         = TRUE;

  $this->load->library('upload', $config);
  $this->upload->do_upload('highlightedBoxImage');
 
  $uploadData = $this->upload->data();
 
  if($uploadData['file_path']!=$uploadData['full_path']){
    
    $data = array
    (   
      'title' => $_POST['title'],
      'content' => $_POST['content'],
      'color' => $_POST['color'],
      'image' =>$uploadData['file_name'],
      'pageId' => $_POST['page']
    ); 

    
  }
  else{
    $data = array
    (
        'title' => $_POST['title'],
        'content' => $_POST['content'],
        'color' => $_POST['color'],
        'pageId' => $_POST['page']
    ); 
  }

  $this->common_model->update('highlighted_box', $id, $data);
  $highlightedBoxData = $this->highlightedbox_model->highlightedBoxFull();
  $pages = $this->common_model->getAllData('pages');

  $tableData=['pageName'=>"HighlightedBox",
              'highlightedBoxData' => $highlightedBoxData,
              'action'  => 'add',
              'options' => $pages
              ];

  $this->load->view('dashboard/highlightedbox', $tableData);

}
} 

?>