<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Banner extends BaseController {


public function __construct()
{
    parent::__construct();
    $this->isLoggedIn();   
}

public function index()
{
   $pages = $this->common_model->getAllData('pages');

   $bannerData = $this->banner_model->bannerFull();
  
   $data=[   'pageName'=>"Banner",
             'bannerData' => $bannerData, 
             'options' => $pages,
             'action'  => 'add'
         ];

  $this->load->view('dashboard/banner',$data);

}

public function add()
{

    $config['upload_path']          = './uploads/';
    $config['allowed_types']        = 'jpg|png|jpeg';
    $config['max_size']             = 5000;
    $config['encrypt_name']         = TRUE;

    $this->load->library('upload', $config);

    $this->upload->do_upload('bannerImage');
    
    $upload_data = $this->upload->data();
    $image_name = $upload_data['file_name'];

    $data = array
    (
        'title' => $_POST['title'],
        'content' =>$_POST['content'],
        'image' => $image_name,
        'pageId' => $_POST['page']
    );  
  
      $this->db->insert('banner', $data);

      $pages = $this->common_model->getAllData('pages');

      $bannerData = $this->banner_model->bannerFull();

      $data=['pageName'=>"Banner",
              'bannerData' => $bannerData, 
              'options' => $pages,
              'check' => 'success',
              'action'  => 'add'
    ];
    $uploadData = $this->upload->data();
    // echo $uploadData['full_path'];
    // echo $uploadData['file_path'];
    // echo $uploadData['file_name'];

      $this->load->view('dashboard/banner',$data);

}

public function delete($id){

  $this->common_model->delete('banner', $id);

  $pages = $this->common_model->getAllData('pages');
  $bannerData = $this->banner_model->bannerFull();
  $data=['pageName'=>"Banner",
          'bannerData' => $bannerData,
          'options' => $pages,
          'action'  => 'add'
      ];
  $this->load->view('dashboard/banner', $data); 

}

public function loadUpdate($id){

  $updateData = $this->common_model->getById('banner',$id);
  $selectedPageData = $this->common_model->getById('pages',$updateData->pageId);
  
  $pages = $this->common_model->getAllData('pages');

  $bannerData = $this->banner_model->bannerFull();

  $tableData=['pageName'=>"Banner",
              'bannerData' => $bannerData,
              'updateData'  => $updateData,
              'action'  => 'update',
              'options' => $pages,
              'selectedPage' => $selectedPageData
              ];

  $this->load->view('dashboard/banner', $tableData);

}

public function update($id){
  
  $config['upload_path']          = './uploads/';
  $config['allowed_types']        = 'jpg|png|jpeg';
  $config['max_size']             = 5000;
  $config['encrypt_name']         = TRUE;

  $this->load->library('upload', $config);
  $this->upload->do_upload('bannerImage');
 
  $uploadData = $this->upload->data();
 
  if($uploadData['file_path']!=$uploadData['full_path']){
    
    $data = array
    (
      'title' => $_POST['title'],
      'content' =>$_POST['content'],
      'image' =>$uploadData['file_name'],
      'pageId' => $_POST['page']
    ); 

    
  }
  else{
    $data = array
    (
      'title' => $_POST['title'],
      'content' =>$_POST['content'],
      'pageId' => $_POST['page']
    ); 
  }

  $this->common_model->update('banner', $id, $data);
  $bannerData = $this->banner_model->bannerFull();
  $pages = $this->common_model->getAllData('pages');

  $tableData=['pageName'=>"Banner",
              'bannerData' => $bannerData ,
              'action'  => 'add',
              'options' => $pages
              ];

  $this->load->view('dashboard/banner', $tableData);

}

public function api(){

  $id = $_POST['id'];
  $data = $this->common_model->getById('banner',$id);
  $pages = $this->banner_model->getpage($id);

  $data->pages= $pages;
  
  $output = json_encode($data);
  echo $output;
}

} 

?>