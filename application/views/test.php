<section id="how-to">
        <!-- Super Accordion -->
      <div class="panel-group" id="superaccordion">
        <!-- Accordion 1 -->
        <div class="panel">
          <div class="panel-heading parent">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#superaccordion" href="#collapse1" aria-expanded="false">
                <img src="https://s3-ap-southeast-1.amazonaws.com/media.go-jek.com/gopay_platformicon/bca.jpg" class="img-responsive" width="100px">
              </a>
          </div>
          <div id="collapse1" class="panel-collapse collapse">
            <div class="panel-body">
              <div class="panel-group" id="accordion1">
                <div class="panel">
                  <div class="panel-heading child">
                    <h4 class="panel-title">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse1A" aria-expanded="false">ATM BCA
                      </a>
                    </h4>
                  </div>
                  <div id="collapse1A" class="panel-collapse collapse">
                    <div class="panel-body">
                      <div class="inside-body">
                        <ol class="list-group vertical-steps">
                          <li class="list-group-item"><span>Masukkan kartu ATM dan PIN BCA Anda</span></li>
                          <li class="list-group-item"><span>Masuk ke menu TRANSFER dan klik BCA Virtual Account</span</li>
                          <li class="list-group-item"><span>Masukkan kode perusahaan untuk GO-JEK: 70001 dan nomor telepon yang terdaftar pada aplikasi (Contoh: 700010812XXXXXX)</span></li>
                          <li class="list-group-item"><span>Masukkan jumlah top up yang diinginkan</span></li>
                          <li class="list-group-item"><span>Ikuti instruksi untuk menyelesaikan transaksi</span></li>
                        </ol>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel">
                  <div class="panel-heading child">
                    <h4 class="panel-title">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse1B" aria-expanded="false">Collapsible Group Item #3.2
                      </a>
                    </h4>
                  </div>
                  <div id="collapse1B" class="panel-collapse collapse">
                    <div class="panel-body">
                      <div class="inside-body">
                      Konten
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- End of Accordion 1 -->
        <!-- Accordion 2 -->
        <div class="panel">
          <div class="panel-heading parent">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#superaccordion" href="#collapse2" aria-expanded="false">
                <img src="https://s3-ap-southeast-1.amazonaws.com/media.go-jek.com/gopay_platformicon/mandiri.jpg" class="img-responsive" width="100px">
              </a>
          </div>
          <div id="collapse2" class="panel-collapse collapse">
            <div class="panel-body">
              <div class="panel-group" id="accordion2">
                <div class="panel">
                  <div class="panel-heading child">
                    <h4 class="panel-title">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse2A" aria-expanded="false">Collapsible Group Item #3.1
                      </a>
                    </h4>
                  </div>
                  <div id="collapse2A" class="panel-collapse collapse">
                    <div class="panel-body">
                      <div class="inside-body">
                        <ol class="list-group vertical-steps">
                          <li class="list-group-item"><span>Masukkan kartu ATM dan PIN BCA Anda</span></li>
                          <li class="list-group-item"><span>Masuk ke menu TRANSFER dan klik BCA Virtual Account</span</li>
                          <li class="list-group-item"><span>Masukkan kode perusahaan untuk GO-JEK: 70001 dan nomor telepon yang terdaftar pada aplikasi (Contoh: 700010812XXXXXX)</span></li>
                          <li class="list-group-item"><span>Masukkan jumlah top up yang diinginkan</span></li>
                          <li class="list-group-item"><span>Ikuti instruksi untuk menyelesaikan transaksi</span></li>
                        </ol>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel">
                  <div class="panel-heading child">
                    <h4 class="panel-title">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse2B" aria-expanded="false">Collapsible Group Item #3.2
                      </a>
                    </h4>
                  </div>
                  <div id="collapse2B" class="panel-collapse collapse">
                    <div class="panel-body">
                      <div class="inside-body">
                      Konten
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- End of Accordion 2 -->
      </div>
      <!-- End of Super Accordion -->
    </section>