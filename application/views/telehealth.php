<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<?php $this->load->view('components/sections/bannerv2'); ?>

<script>
    function telehealthCount() {
        $.ajax({
            url: '<?php echo base_url('web/api/telehealth') ?>', 
            type:'get',
            dataType: 'json',
            success: function(results){ 
                $('#telehealthCount').html(results);            
            },
        
            error:function(){
                console.log('error');
            }
        });
    }

    $(document).ready(function(){
        myVar = setInterval("telehealthCount()", 30000);
    });
</script>

<div class="container">
    <div class="row">
        <div class="col-lg-12 pt-5 text-center">
            <p class="font26 b600">Connect 24/7 through Telehealth</p>
            <p class="font20">We are now offering 24/7 Telehealth appointments during COVID -19 pandemic.</p>

            <div class="row">
                <div class="col-lg-3 text-center d-none d-lg-block" style="margin-top: 125px;">
                    <p>Connect via text</p>
                    <div>
                        <a href="sms:6262987121">
                            <div class="teleheath-purple-button-e text-center">
                                English<span class="purple-button-num"> (626) 298-7121</span>
                            </div>
                        </a>
                        <a href="sms:6262697744">
                            <div class="teleheath-purple-button text-center">
                                Spanish<span class="purple-button-num"> (626) 269-7744</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 text-center d-block d-lg-none my-4">
                    <p>Connect via text</p>
                    <div>
                        <a href="sms:6262987121">
                            <div class="teleheath-purple-button-e text-center">
                                English<span class="purple-button-num"> (626) 298-7121</span>
                            </div>
                        </a>
                        <a href="sms:6262697744">
                            <div class="teleheath-purple-button text-center">
                                Spanish<span class="purple-button-num"> (626) 269-7744</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 text-center telehealth-button-section">
                    <p>Connect via Doxy <br />We are open for visits by appointment</p>
                    
                    <div class="doxyme-badge doxyme-badge-horizontal m-auto d-none d-md-block">
                        <div class="doxyme-badge-horizontal-image">
                            <img src="https://www.gravatar.com/avatar/94de776c214a2fbb7378bec74316309b?s=240"/>
                        </div>

                        <div class="doxyme-badge-indicator" data-subscribe-key="sub-c-4c711c18-7e53-11e5-8d3c-0619f8945a4f" data-uuid="d2160d20-721d-11ea-9b16-5519e2e14a15:web" data-channel="room--461645" data-institution-id="5128" data-api-url="https://api.doxy.me"></div>

                        <div class="doxyme-badge-content">
                            <div class="doxyme-badge-start-a-call"></div>

                            <a class="doxyme-badge-start-a-call" href="https://KTDOCTOR.doxy.me/kttelehealth" target="_blank">Start a telemedicine call with</a>
                            <div class="doxyme-badge-doctor-name">
                                KT Telehealth
                            </div>

                            <div>
                                <a class="doxyme-badge-start-button btn-primary" href="https://KTDOCTOR.doxy.me/kttelehealth" target="_blank">Enter waiting room</a>
                            </div>

                            <div>
                                <div class="doxyme-badge-powered-by">
                                    <a href="https://doxy.me" target="_blank">Telemedicine</a> by
                                </div>
                                <div class="doxyme-badge-logo-link">
                                    <a href="https://doxy.me" target="_blank">Doxy.me</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="doxyme-badge doxyme-badge-vertical m-auto d-block d-md-none">
                        <div class="doxyme-badge-start-a-call-div">
                            <a class="doxyme-badge-start-a-call" href="https://KTDOCTOR.doxy.me/kttelehealth" target="_blank">Start a telemedicine call with</a>
                        </div>

                        <div class="doxyme-badge-doctor-name">KT Telehealth</div>

                        <div class="doxyme-badge-vertical-image">
                            <img src="https://www.gravatar.com/avatar/94de776c214a2fbb7378bec74316309b?s=240"/>
                            <span class="doxyme-badge-indicator" data-subscribe-key="sub-c-4c711c18-7e53-11e5-8d3c-0619f8945a4f" data-uuid="d2160d20-721d-11ea-9b16-5519e2e14a15:web" data-channel="room--461645" data-api-url="https://api.doxy.me"></span>
                        </div>

                        <div class="doxyme-badge-vertical-footer">
                            <div class="doxyme-badge-powered-by">
                                <a href="https://doxy.me" target="_blank">Telemedicine</a> by
                            </div>

                            <div class="doxyme-badge-logo-link-large">
                                <a href="https://doxy.me" target="_blank">Doxy.me</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 telehealth-button-section text-center d-none d-lg-block" style="margin-top: 125px;">
                    <p>Connect via Facebook</p>
                    <div>
                        <a href="https://www.messenger.com/login.php?next=https%3A%2F%2Fwww.messenger.com%2Ft%2F141266270391%2F%3Fmessaging_source%3Dsource%253Apages%253Amessage_shortlink" target="blank">
                            <div class="teleheath-mezenger-button text-center">
                                <i class="fab fa-facebook-messenger"></i> Send to Messenger
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 telehealth-button-section text-center d-block d-lg-none my-4" style="margin-top: 125px;">
                    <p>Connect via Facebook</p>
                    <div>
                        <a href="https://www.messenger.com/login.php?next=https%3A%2F%2Fwww.messenger.com%2Ft%2F141266270391%2F%3Fmessaging_source%3Dsource%253Apages%253Amessage_shortlink" target="blank">
                            <div class="teleheath-mezenger-button text-center">
                                <i class="fab fa-facebook-messenger"></i> Send to Messenger
                            </div>
                        </a>
                    </div>
                </div>
            </div>    

            <p class="font20 text-center mt-4">
                If you are having a medical emergency, call 911 for emergency medical help. Our team members who will care for your child have the expertise and skills to provide the best care possible.
            </p>

            <hr>
            <h3 class="telehealth-count-text">Over <span class="telehealth-count" id="telehealthCount"><?php echo $count?></span> Telehealth Apointments Today</h3>
        </div>

        <!-- <div class="col-lg-4 p-0 py-5 appointment-right-contact-area">
            <?php $this->load->view('components/sections/telehealth'); ?>
        </div> -->
    </div>
</div>

<?php $this->load->view('components/sections/textBox'); ?>
<?php $this->load->view('components/sections/descriptionBox'); ?>
<?php $this->load->view('components/common/footer'); ?>