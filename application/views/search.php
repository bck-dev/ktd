<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>

<div class="container py-7">
    <div class="row pt-6">
        <div class="col-12 mb-5">
            <h1><?php if($count>0): echo $count; else: echo "0"; endif; ?>&nbsp;Search results for "<?php echo $keyword ?>"</h1>
        </div>
        <?php foreach($location_results as $result): ?>
            <div class="col-12">
                <div class="card result mb-2">
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $result->name; ?></h5>
                        <h6 class="card-subtitle mb-1 text-muted">
                            Address: <span class="purple mr-4"> <?php echo $result->address ?></span>
                        </h6>
                        <p class="card-text text-muted">
                            Telephone: <span class="purple mr-4"> <?php echo $result->telephone ?></span> 
                            Fax: <span class="purple mr-4"> <?php echo $result->fax ?></span>
                        </p>
                        <a href="<?php echo base_url('locations'); ?>" class="btn btn-link font16 b600 p-0">Go to locations page</a>
                    </div>
                </div> 
            </div>
        <?php endforeach; ?>

        <?php foreach($doctor_results as $result): ?>
            <div class="col-12">
                <div class="card result mb-2">
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $result->name; ?> <b><?php echo $result->qualification; ?></b></h5>
                        <h6 class="card-subtitle mb-1 text-muted">
                            Specialty: <span class="purple mr-4"> <?php echo $result->specialty ?></span>
                        </h6>
                        <a href="<?php echo base_url('doctors'); ?>" class="btn btn-link font16 b600 p-0">Go to doctors page</a>
                    </div>
                </div> 
            </div>
        <?php endforeach; ?>
    </div>
</div>

<?php $this->load->view('components/common/footer'); ?>