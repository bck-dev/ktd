<footer>
    <div class="dark_purple_background py-6">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <h3 class="font18 white">Kids & Teens Medical Group</h3>
                    <p class="grey font15 mb-5 pr-5">
                        Kids and Teens Medical Group is the leading provider for pediatric care with extended hours in the greater Los Angeles area. We are focused on changing the way you think about health of children.
                    </p>
                </div>
                <div class="col-lg-3 ">
                    <p class="font18 white">For Patients</p>
                    <ul class="grey font15">
                        <li><a href="https://patientportal.intelichart.com/login/Account/Login?ReturnUrl=%2f" target="_blank" class="grey">Patient Portal</a></li>
                        <li><a href="<?php echo base_url('appointment');?>" class="grey">Request an appointment</a></li>
                        <li><a href="<?php echo base_url('doctors');?>" class="grey">Find a doctor</a></li>
                        <li><a href="<?php echo base_url('insurance');?>" class="grey">Insurance</a></li>
                        <li><a href="<?php echo base_url('locations');?>" class="grey">Locations</a></li>
                        <li><a href="<?php echo base_url('existing');?>" class="grey">Existing Patients</a></li>
                        <li><a href="<?php echo base_url('faq');?>" class="grey">FAQ</a></li>
                    </ul>
                    <p class="font18 white">For Medical Professionals</p>
                    <ul class="grey font15 mb-5">
                        <li><a href="<?php echo base_url('practice');?>" class="grey" >Practice Opportunities</a></li>
                        <li><a href="<?php echo base_url('locations');?>" class="grey">Locations</a></li>
                    </ul>
                </div>
                <div class="col-lg-3">
                    <p class="font18 white">For Parents</p>
                    <ul class="grey font15">
                        <li><a href="https://patientportal.intelichart.com/login/Account/Login?ReturnUrl=%2f" target="_blank" class="grey">Patient Portal</a></li>
                        <li><a href="<?php echo base_url('appointment');?>" class="grey">Request an appointment</a></li>
                        <li><a href="<?php echo base_url('insurance');?>" class="grey">Insurance</a></li>
                        <li><a href="<?php echo base_url('expectantmothers');?>" class="grey">Expectant Mothers</a></li>
                        <li><a href="<?php echo base_url('newborncare');?>" class="grey">New Born Care</a></li>
                        <li><a href="<?php echo base_url('vaccinations');?>" class="grey">Vaccinations and Immunizations</a></li>
                    </ul>
                    <p class="font18 white">Careers</p>
                    <ul class="grey font15">
                        <li><a href="<?php echo base_url('career');?>" class="grey">Vacancies</a></li>
                        
                    </ul>
                    <p class="font18 white">Staff</p>
                    <ul class="grey font15">
                        <li><a href="https://login.microsoftonline.com/common/oauth2/authorize?client_id=4345a7b9-9a63-4910-a426-35363201d503&redirect_uri=https%3A%2F%2Fwww.office.com%2Flanding&response_type=code%20id_token&scope=openid%20profile&response_mode=form_post&nonce=637175224669892996.YmY2NDBhMzktYjUxNC00ZDkwLTlkNjgtM2Y4YmRjNGQyZDI1ZGM3NjI0OTAtMzE0NC00OTM1LWE2OWUtNzcyZWE0M2U1YzMx&ui_locales=en-US&mkt=en-US&client-request-id=1126c4cc-4135-4834-bddd-15ef21e48c93&state=vnpDxQn0wfMSMRP7TqJuKUMqVnr3dZR-mxVDmU1h6LVLQorzMKVtmx5Ct-qubs0vQ7vE4HCZrBLLhViuX-Sksrq5z3a3D0Ns8MLUC6MGxdW0dg5pApqPkpQxaMlBtBUgimK_3WIFrJnAfNPXlCBVw8w4_EhOT7iGgYx8t_WJWRl_-5QcDIkl_eKoI9eMSh-RYTp3JzdjqQMxD9OYwiqPEXw8bk07EjfvLmsQvtiIYWNimBcrhoDRUCeBekXQk78Xicn1CfVJrEa8zSTk2u24T2KWxPA4ApnuPfX-uVXYczIqGvZbmJDCHBuEjeayHejx1dBLXqBeqKD1i5Nqu3tuUTZTgqnU2y8IRxAGMYcR3MJELkdiVUCZNYw9ZcDiTHDKWEpwWvCnvREhSxi9lmj3YCBTF-0Cc1VjPmpWa4yN0em7KXpooxcV67_6ssEpkOfTTfNGkpNGHS5gMxjxwMuuGEC7NdALV_zonxRwA_4Pj8u_EQ5jMZbUwkSGzS_V9Fr_V_8bQNK9LSCEqFL92d94JikRmUs5Bv8s1saNEjYu-SgOJ_qO_VRbgqXjdB91smsDJGH-7YX-JA-Pz8nqzoVkdebgKacXG3tZvElNFnQc-1N28kyjNem35ErDbqtCKsSmci7gQ2rSBD9wPQTuz0tj7A&x-client-SKU=ID_NETSTANDARD2_0&x-client-ver=6.3.0.0&sso_reload=true" target="_blank" class="grey">Staff Access</a></li>
                    </ul>
                </div>
                <div class="col-lg-3">
                    <p class="font18 white">Stay Connected</p>
                    <p class="font15 white b600">Corporate Office</p>
                    <p class="grey font15">504 S. Sierra Madre Blvd <br />Pasadena, CA 91107</p>
                    <p class="grey font15">
                        Call <a href="tel:8183615437" class="grey">(818) 361-5437</a> <br />
                        Text (E) <a href="sms:6262987121" class="grey">(626) 298-7121 </a><br />
                        Text (S) <a href="sms:6262697744" class="grey">(626) 269-7744 </a><br />
                        <a href="mailto:info@ktdoctor.com" class="grey">info@ktdoctor.com </a><br />
                    </p>
                    <p class="font15 white b600">Social Media</p>
                    <a href="http://facebook.com/kidsandteensmedicalgroup/" target="_blank" class="font26 icon yellow p-1">
                        <i class="fab fa-facebook-f"></i>                            
                    </a>
                    <a href="https://www.instagram.com/ktmedicalgroup/" target="_blank" class="font26 icon yellow p-1">
                        <i class="fab fa-instagram"></i>                           
                    </a>
                    <a href="https://www.youtube.com/channel/UC5pMXGZ_F2OZUFdfy6YbIew" target="_blank" class="font26 icon yellow p-1">
                        <i class="fab fa-youtube"></i>                      
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="py-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-7 footer-bottom-left-text">                    
                    <!-- <ul class="dark_purple bottom_bar">
                        <li>Terms and Conditions</li>
                        <li>Privacy Policy</li>
                        <li>Notice of Privacy Practices</li>
                    </ul> -->
                    <p class="dark_purple font12 m-0">© 2020 Kids & Teens Medical Group (KTDoctor). All rights reserved.</p>
                </div>
                <div class="col-lg-4 col-md-5 footer-bottom-right-text">
                    <a href="http://www.bckonnect.com" class="dark_purple font12 m-0" target="_blank">Website by BCKonnect</a>
                </div>
            </div>
        </div>
    </div>

</footer>


<?php $this->load->view('components/common/social'); ?>

<script src="<?php echo base_url('assets/slick/slick.js'); ?>"></script>	
<script src="<?php echo base_url('assets/common/js/popper.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>  
<script src="<?php echo base_url('assets/gijgo/gijgo.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/ktd/js/main.js'); ?>"></script>	
<script src="<?php echo base_url('assets/jvalid/dist/jquery.validate.js'); ?>"></script>

</body>
</html>