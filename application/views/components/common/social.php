<style>

.icon-bar {
  position: fixed;
  top: 52%;
  right: 0;
  -webkit-transform: translateY(-70%);
  -ms-transform: translateY(-70%);
  transform: translateY(-70%);
}

.icon-bar a {
  display: block;
  text-align: center;
  padding: 16px;
  transition: all 0.3s ease;
  color: white;
  font-size: 20px;
}

.icon-bar a:hover {
  background-color: #000;
}

.facebook {
  background: #3B5998;
  color: white;
}

.twitter {
  background: #55ACEE;
  color: white;
}

.google {
  background: #dd4b39;
  color: white;
}

.linkedin {
  background: #007bb5;
  color: white;
}

.youtube {
  background: #bb0000;
  color: white;
}

.staff-access {
  background: #F6C711;
  color: white;
}

.content {
  margin-left: 75px;
  font-size: 30px;
}

@media only screen and (min-width: 768px) and (max-width: 1024px){
  .icon-bar {
    position: fixed;
    top: 80%;
    right: 0;
    -webkit-transform: translateY(-70%);
    -ms-transform: translateY(-70%);
    transform: translateY(-70%);
  }
}
</style>


<div class="icon-bar d-none d-lg-block">
  <a href="https://www.youtube.com/channel/UC5pMXGZ_F2OZUFdfy6YbIew" target="_blank" class="youtube"><i class="fab fa-youtube"></i></a>
  <a href="http://facebook.com/kidsandteensmedicalgroup/" target="_blank" class="facebook"><i class="fab fa-facebook-f"></i></a> 
  <a href="https://www.instagram.com/ktmedicalgroup/" target="_blank" class="twitter"><i class="fab fa-instagram"></i></a>
  <a href="https://login.microsoftonline.com/common/oauth2/authorize?client_id=4345a7b9-9a63-4910-a426-35363201d503&redirect_uri=https%3A%2F%2Fwww.office.com%2Flanding&response_type=code%20id_token&scope=openid%20profile&response_mode=form_post&nonce=637175224669892996.YmY2NDBhMzktYjUxNC00ZDkwLTlkNjgtM2Y4YmRjNGQyZDI1ZGM3NjI0OTAtMzE0NC00OTM1LWE2OWUtNzcyZWE0M2U1YzMx&ui_locales=en-US&mkt=en-US&client-request-id=1126c4cc-4135-4834-bddd-15ef21e48c93&state=vnpDxQn0wfMSMRP7TqJuKUMqVnr3dZR-mxVDmU1h6LVLQorzMKVtmx5Ct-qubs0vQ7vE4HCZrBLLhViuX-Sksrq5z3a3D0Ns8MLUC6MGxdW0dg5pApqPkpQxaMlBtBUgimK_3WIFrJnAfNPXlCBVw8w4_EhOT7iGgYx8t_WJWRl_-5QcDIkl_eKoI9eMSh-RYTp3JzdjqQMxD9OYwiqPEXw8bk07EjfvLmsQvtiIYWNimBcrhoDRUCeBekXQk78Xicn1CfVJrEa8zSTk2u24T2KWxPA4ApnuPfX-uVXYczIqGvZbmJDCHBuEjeayHejx1dBLXqBeqKD1i5Nqu3tuUTZTgqnU2y8IRxAGMYcR3MJELkdiVUCZNYw9ZcDiTHDKWEpwWvCnvREhSxi9lmj3YCBTF-0Cc1VjPmpWa4yN0em7KXpooxcV67_6ssEpkOfTTfNGkpNGHS5gMxjxwMuuGEC7NdALV_zonxRwA_4Pj8u_EQ5jMZbUwkSGzS_V9Fr_V_8bQNK9LSCEqFL92d94JikRmUs5Bv8s1saNEjYu-SgOJ_qO_VRbgqXjdB91smsDJGH-7YX-JA-Pz8nqzoVkdebgKacXG3tZvElNFnQc-1N28kyjNem35ErDbqtCKsSmci7gQ2rSBD9wPQTuz0tj7A&x-client-SKU=ID_NETSTANDARD2_0&x-client-ver=6.3.0.0&sso_reload=true" target="_blank" class="staff-access"><img src="assets/images/users.png" title="Staff Access" width="26"></i></a>
</div>
