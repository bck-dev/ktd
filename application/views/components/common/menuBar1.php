<div class="menu <?php if($pageName!="Home"){ echo'menu_other';} ?>">

    <div class="top_bar light_blue_background p-2">
        <div class="container d-none d-lg-block">
            <div class="row c-2">
                <!--
                <div class="col-6 col-sm-4 col-lg-1 pt-2">
                    
                    <a href="#" onclick="doGTranslate('en|en');return false;" title="English" class="gflag nturl m-0 p-0" style="background-position:-0px -0px;"><img src="//gtranslate.net/flags/blank.png" height="24" width="24" alt="English" style="margin: 0px;"/></a>
                    <a href="#" onclick="doGTranslate('en|es');return false;" title="Spanish" class="gflag nturl m-0 p-0" style="background-position:-600px -200px;"><img src="//gtranslate.net/flags/blank.png" height="24" width="24" alt="Spanish"  style="margin: 0px;"/></a>
                    <select onchange="doGTranslate(this);" style="display: none;"><option value="">Select Language</option><option value="en|af">Afrikaans</option><option value="en|sq">Albanian</option><option value="en|ar">Arabic</option><option value="en|hy">Armenian</option><option value="en|az">Azerbaijani</option><option value="en|eu">Basque</option><option value="en|be">Belarusian</option><option value="en|bg">Bulgarian</option><option value="en|ca">Catalan</option><option value="en|zh-CN">Chinese (Simplified)</option><option value="en|zh-TW">Chinese (Traditional)</option><option value="en|hr">Croatian</option><option value="en|cs">Czech</option><option value="en|da">Danish</option><option value="en|nl">Dutch</option><option value="en|en">English</option><option value="en|et">Estonian</option><option value="en|tl">Filipino</option><option value="en|fi">Finnish</option><option value="en|fr">French</option><option value="en|gl">Galician</option><option value="en|ka">Georgian</option><option value="en|de">German</option><option value="en|el">Greek</option><option value="en|ht">Haitian Creole</option><option value="en|iw">Hebrew</option><option value="en|hi">Hindi</option><option value="en|hu">Hungarian</option><option value="en|is">Icelandic</option><option value="en|id">Indonesian</option><option value="en|ga">Irish</option><option value="en|it">Italian</option><option value="en|ja">Japanese</option><option value="en|ko">Korean</option><option value="en|lv">Latvian</option><option value="en|lt">Lithuanian</option><option value="en|mk">Macedonian</option><option value="en|ms">Malay</option><option value="en|mt">Maltese</option><option value="en|no">Norwegian</option><option value="en|fa">Persian</option><option value="en|pl">Polish</option><option value="en|pt">Portuguese</option><option value="en|ro">Romanian</option><option value="en|ru">Russian</option><option value="en|sr">Serbian</option><option value="en|sk">Slovak</option><option value="en|sl">Slovenian</option><option value="en|es">Spanish</option><option value="en|sw">Swahili</option><option value="en|sv">Swedish</option><option value="en|th">Thai</option><option value="en|tr">Turkish</option><option value="en|uk">Ukrainian</option><option value="en|ur">Urdu</option><option value="en|vi">Vietnamese</option><option value="en|cy">Welsh</option><option value="en|yi">Yiddish</option></select><div id="google_translate_element2"></div>

                </div>
                -->
                <div class="col-xl-7 col-sm-4 col-lg-6 d-flex">
                    <a href="./contact" class="font13 mr-1">
                        <div class="d-flex">
                            <div class="top_bar_item">
                                <i class="fas fa-users mr-2 font20"></i>
                            </div>
                            <div class="top_bar_item staffaccess-separator"> 
                                Contact
                            </div>
                        </div>
                    </a>
                    <a href="https://pay.instamed.com/Form/PaymentPortal/Default?id=PEDAFFILIATES" target="_blank" class="font13 mr-3">
                        <div class="d-flex pl-md-3">
                            <div class="top_bar_item">
                                <i class="fas fa-credit-card mr-2 font20"></i>
                            </div>
                            <div class="top_bar_item"> 
                                PAY YOUR BILL
                            </div>
                        </div>
                    </a>
                    <a href="https://patientportal.intelichart.com/login/Account/Login?ReturnUrl=%2f" target="_blank" class="font13">
                        <div class="d-flex">
                            <div class="top_bar_item">
                                <i class="fas fa-medkit mr-2 font20"></i>
                            </div>
                            <div class="top_bar_item"> 
                                PATIENT PORTAL
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xl-5 col-lg-6 col-sm-4 d-flex p-0"  style="justify-content: space-between; margin-top: -1px;">                        
                    <div class="text-right white b600 mt-1 textline_top pt-1 header-top-text"> 
                        Text Us at &nbsp; (E) <a class="header-top-text" href="sms:6262987121">(626) 298-7121 </a>
                        &nbsp;(S) <a class="header-top-text" href="sms:6262697744">(626) 269-7744 </a>
                    </div>
                    
                    <div class="top_bar_item p-0 float-right"> 
                        <a href="<?php echo base_url('appointment'); ?>" class="btn btn-light btn-block font15" style="color: #42b9ff; height: auto; padding:5px 10px;">APPOINTMENT</a>
                    </div>          
                </div>                  
            </div>
        </div>
        <div class="container d-block d-lg-none">
            <div class="row">
                <div class="col-6 col-md-9 p-0">
                    <a href="<?php echo base_url('contact'); ?>" class="font13">
                        <div class="d-flex">
                            <div class="top_bar_item">
                                <i class="fas fa-headset mr-2 font20"></i>
                            </div>
                            <div class="top_bar_item font15"> 
                                CONTACT US
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-3 p-0">
                    <div class="d-flex">
                        <div class="top_bar_item">
                            <i class="far fa-comments white mr-2 font20"></i>
                        </div>
                        <div class="top_bar_item font15 white mt-2 text-right"> 
                            <p>
                                (E) <a href="sms:6262987121">(626) 298-7121 </a><br />
                                (S) <a href="sms:6262697744">(626) 269-7744 </a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-9 p-0">
                    <a href="https://pay.instamed.com/Form/PaymentPortal/Default?id=PEDAFFILIATES" target="_blank" class="font13">
                        <div class="d-flex">
                            <div class="top_bar_item">
                                <i class="fas fa-credit-card mr-2 font20"></i>
                            </div>
                            <div class="top_bar_item font15"> 
                                PAY YOUR BILL
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-md-3 p-0">
                    <a href="https://patientportal.intelichart.com/login/Account/Login?ReturnUrl=%2f" target="_blank" class="font13">
                        <div class="d-flex">
                            <div class="top_bar_item">
                                <i class="fas fa-medkit mr-2 font20"></i>
                            </div>
                            <div class="top_bar_item font15"> 
                                PATIENT PORTAL
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        
    </div>
    <a class="d-block d-lg-none" href="<?php echo base_url('appointment'); ?>">
        <div class="yellow_background p-2 w-100 black b600 font20 text-center">
            Appointment
        </div> 
    </a>
    <div class="container">
        <nav class="navbar navbar-expand-lg ktd_nav pt-2 d-none d-lg-block">
            <a class="navbar-brand" href="<?php echo base_url(''); ?>">
                <img src="<?php echo base_url('assets/images/logo.png'); ?>" alt="logo" class="logo">
            </a>
            <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><i class="fas fa-bars nav-menu-toggler"></i></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link <?php if($pageName=="Home"):?>purple<?php endif; ?>" href="<?php echo base_url(''); ?>">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if($pageName=="Doctors"):?>purple<?php endif; ?>" href="<?php echo base_url(''); ?>doctors">Doctors</a>
                </li>            
                <li class="nav-item dropdown megamenu" id="patientCare">
                    <a id="megamneu" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link  <?php if($pageName=="Expectant Mothers" OR $pageName=="New Born Care"  OR $pageName=="Practice Opportunities"  OR $pageName=="Existing Patients"  OR $pageName=="Vaccinations and Immunization"):?>purple<?php endif; ?>">Patients Care</a>
                    <div aria-labelledby="megamneu" class="dropdown-menu border-0 p-0 m-0" id="patientCareDrop">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-4 offset-lg-5 purple_background p-3 px-4">
                                    <ul class="megaul">
                                        <li><a class="white" href="<?php echo base_url('expectantmothers'); ?>">Expectant Mothers</a></li>
                                        <li><a class="white" href="<?php echo base_url('newborncare'); ?>">Newborn Care</a></li>
                                        <hr style="border-bottom: #4C3273 solid 1px;"/>
                                        <li><a class="white" href="<?php echo base_url('practice'); ?>">Practice Opportunities</a></li>
                                        <li><a class="white" href="<?php echo base_url('existing'); ?>">Existing Patients</a></li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 white_background p-3 pl-4">
                                    <a href="<?php echo base_url('afterhour'); ?>"><p class="font26 light_blue b300">After Hours Care <i class="fas fa-chevron-right ml-1 font20 purple"></i></p></a>
                                    <p>More information about extended hours of operation</p>
                                    <hr />
                                    <a href="<?php echo base_url('vaccinations'); ?>"><p class="font26 light_blue b300">Vaccination & Immunizations <i class="fas fa-chevron-right ml-1 font20 purple"></i></p></a>
                                    <p>More information about vaccinations and immunization for your child</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>            
                <li class="nav-item">
                    <a class="nav-link <?php if($pageName=="Insurance"):?>purple<?php endif; ?>" href="<?php echo base_url('insurance'); ?>">Insurance</a>
                </li>            
                <li class="nav-item">
                    <a class="nav-link <?php if($pageName=="About Us"):?>purple<?php endif; ?>" href="<?php echo base_url('about'); ?>">About Us</a>
                </li>            
                <li class="nav-item">
                    <a class="nav-link <?php if($pageName=="Locations"):?>purple<?php endif; ?>" href="<?php echo base_url('locations'); ?>">Locations</a>
                </li>

                <!-- <li class="nav-item">
                    <a class="nav-link <?php if($pageName=="contact"):?>purple<?php endif; ?>" href="<?php echo base_url('contact'); ?>">Contact Us</a>
                </li> -->
                         
                <li class="nav-item d-none appointment_nav" id="appointmentMenu">
                    <a class="nav-link appointment_menu" href="<?php echo base_url('appointment'); ?>">Appointment</a>
                </li>
                
                </ul>
            </div>
        </nav>

        <nav class="navbar navbar-expand-lg ktd_nav pt-2 d-block d-lg-none">
            <a class="navbar-brand" href="<?php echo base_url(''); ?>">
                <img src="<?php echo base_url('assets/images/logo.png'); ?>" alt="logo" class="logo">
            </a>
            <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContentMobile" aria-controls="navbarSupportedContentMobile" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><i class="fas fa-bars nav-menu-toggler"></i></span>
            </button>

            <div class="collapse navbar-collapse white_background" id="navbarSupportedContentMobile">
                <ul class="navbar-nav ml-auto">
                <li class="nav-item active mob">
                    <a class="nav-link" href="<?php echo base_url(''); ?>">Home</a>
                </li>
                <li class="nav-item mob">
                    <a class="nav-link" href="<?php echo base_url(''); ?>doctors">Doctors</a>
                </li>            
                <li class="nav-item dropdown megamenu mob" id="patientCareMob"><a id="megamneu" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link">Patients Care</a>
                    <div aria-labelledby="megamneu" class="dropdown-menu border-0 p-0 m-0" id="patientCareDropMob">
                        <div class="container mobile_shadow">
                            <div class="row">
                                <div class="col-lg-12 purple_background p-3">
                                    <ul class="megaul">
                                        <li><a class="white" href="<?php echo base_url('expectantmothers'); ?>">Expectant Mothers</a></li>
                                        <li><a class="white" href="<?php echo base_url('newborncare'); ?>">Newborn Care</a></li>
                                        <li><a class="white" href="<?php echo base_url('practice'); ?>">Practice Opportunities</a></li>
                                        <li><a class="white" href="<?php echo base_url('existing'); ?>">Existing Patients</a></li>
                                    </ul>
                                </div>
                                <div class="col-lg-12 white_background py-2 px-3">
                                    <a href="<?php echo base_url('afterhour'); ?>"><p class="font18 light_blue b300 m-0">After Hours Care <i class="fas fa-chevron-right ml-1 font16 purple"></i></p></a>
                                    <p class="font13 m-0">More information about extended hours of operation</p>
                                    <hr />
                                    <a href="<?php echo base_url('vaccinations'); ?>"><p class="font18 light_blue b300 m-0">Vaccination & Immunizations <i class="fas fa-chevron-right ml-1 font16 purple"></i></p></a>
                                    <p class="font13 m-0">More information about vaccinations and immunization for your child</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>            
                <li class="nav-item mob">
                    <a class="nav-link" href="<?php echo base_url('insurance'); ?>">Insurance</a>
                </li>            
                <li class="nav-item mob">
                    <a class="nav-link" href="<?php echo base_url('about'); ?>">About Us</a>
                </li>            
                <li class="nav-item mob">
                    <a class="nav-link" href="<?php echo base_url('locations'); ?>">Locations</a>
                </li>
                
                </ul>
            </div>
        </nav>
    </div>

</div>

<!-- GTranslate: https://gtranslate.io/ -->

<!-- <style type="text/css">

a.gflag {vertical-align:middle;font-size:16px;padding:1px 0;background-repeat:no-repeat;background-image:url(//gtranslate.net/flags/16.png);}
a.gflag img {border:0;}
a.gflag:hover {background-image:url(//gtranslate.net/flags/16a.png);}
#goog-gt-tt {display:none !important;}
.goog-te-banner-frame {display:none !important;}
.goog-te-menu-value:hover {text-decoration:none !important;}
body {top:0 !important;}
#google_translate_element2 {display:none!important;}

</style>

<script type="text/javascript">
function googleTranslateElementInit2() {new google.translate.TranslateElement({pageLanguage: 'en',autoDisplay: false}, 'google_translate_element2');}
</script><script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>


<script type="text/javascript">
/* <![CDATA[ */
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('6 7(a,b){n{4(2.9){3 c=2.9("o");c.p(b,f,f);a.q(c)}g{3 c=2.r();a.s(\'t\'+b,c)}}u(e){}}6 h(a){4(a.8)a=a.8;4(a==\'\')v;3 b=a.w(\'|\')[1];3 c;3 d=2.x(\'y\');z(3 i=0;i<d.5;i++)4(d[i].A==\'B-C-D\')c=d[i];4(2.j(\'k\')==E||2.j(\'k\').l.5==0||c.5==0||c.l.5==0){F(6(){h(a)},G)}g{c.8=b;7(c,\'m\');7(c,\'m\')}}',43,43,'||document|var|if|length|function|GTranslateFireEvent|value|createEvent||||||true|else|doGTranslate||getElementById|google_translate_element2|innerHTML|change|try|HTMLEvents|initEvent|dispatchEvent|createEventObject|fireEvent|on|catch|return|split|getElementsByTagName|select|for|className|goog|te|combo|null|setTimeout|500'.split('|'),0,{}))
/* ]]> */
</script> -->
