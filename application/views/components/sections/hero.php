<?php $i=1; foreach($banner as $b): ?>
    <?php if($i==1):?>
        <div class="hero" style="background-image:linear-gradient(to bottom, rgba(255, 255, 255, 0.8), rgba(255, 255, 255, 0)), url('<?php echo base_url();?>uploads/<?php echo $b->image; ?>'); ">
            <div class="container p-0 pt-7">
                <div class="news-marquee-area">
                    <div class="news-marquee" data-duplicated='true' data-direction='left'>
                        <?php $x=1; foreach($sliders as $s): ?>
                            <span class="mr-3 news-text"><b><?php echo $s->title; ?> </b><?php $strnosp = str_replace('<p>', '', $s->content);  $strnoep = str_replace('</p>', '', $strnosp); $strnobr = str_replace('<br>', '', $strnoep); echo $strnobr; ?></span>
                        <?php $x++; endforeach; ?>
                    </div>
                </div>
                <!-- <div class="hero_slogan_box pb-5">
                    <h1 class="hero_slogan b600">Classic Pediatrics <br />
                        <span class="b300 dark_grey">Offering 24/7 <a href="<?php echo base_url('appointment'); ?>" class="b600 light_blue hero_a">Telehealth</a> Appointments And Well Visits for Vaccines</span>
                    </h1>
                </div> --> 
                <div class="row hero_btnset">
                    <!-- <div class="col-6 col-lg-3 col-md-6 col-xs-6 mt-2">
                        <div class="hero_btn dark_grey font15 hero_big_btn yellow_background purple b600">
                            <div class="w-100 d-flex">
                                <div class="col-2 p-0">
                                    <img src="assets/images/24h.png" class="telehealth-img">
                                </div>
                                <div class="col-10 p-0">
                                    <a href="<?php echo base_url('appointment'); ?>"><h3 class="telehealth-heading">Telehealth</h3></a>
                                    <p class="telehealth-text">Text Us</p>
                                    <span>
                                        <a href="sms:6262987121" class="black"><p class="telehealth-contact"><span class="telehealth-contact-text">English</span> (626) 298-7121</p></a> 
                                        <a href="sms:6262697744" class="black"><p class="telehealth-contact"><span class="telehealth-contact-text">Spanish</span> (626) 269-7744</p></a>
                                    <span>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-6 col-lg-3 col-md-6 col-xs-6 mt-2">
                        <div class="hero-appointment-btn">
                        <a href="<?php echo base_url('appointment'); ?>">
                            <div class="hero_btn dark_grey hero-appointment-button-bg">Request Appointment<br /><span class="b600 font24">Telehealth <span class="b400">or</span> In-Office</span>
                            </div>
                        </a>
                        </div>
                    </div>

                    <div class="col-6 col-lg-3 col-md-6 col-xs-6 mt-2">
                        <a href="<?php echo base_url('doctors'); ?>">
                            <div class="hero_btn dark_grey">Board Certified <br /><span class="b600 font24">PHYSICIANS</span></div>
                        </a>
                    </div>
                    <div class="col-6 col-lg-3 col-md-6 col-xs-6 mt-2">
                        <a href="https://www.pediatricafterhour.com/" target="_blank">
                            <div class="hero_btn dark_grey">Urgent Care <br /><span class="b600 font24">AFTER HOURS</span></div>
                        </a>
                    </div>
                    <div class="col-6 col-lg-3 col-md-6 col-xs-6 mt-2">
                        <a href="<?php echo base_url('insurance'); ?>">
                            <div class="hero_btn dark_grey">Accepts Most <br /><span class="b600 font24">INSURANCE</span></div>
                        </a>
                    </div>
                </div>               

            </div>
        </div>
    <?php endif; ?>  
<?php $i++; endforeach; ?> 

<script>
    $('.news-marquee').marquee({
  direction: 'left',
  duration: 10000,
});
</script>