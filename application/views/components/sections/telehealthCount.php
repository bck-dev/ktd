<script>
    function telehealthCount() {
        $.ajax({
            url: '<?php echo base_url('web/api/telehealth') ?>', 
            type:'get',
            dataType: 'json',
            success: function(results){ 
                $('#telehealthCount').html(results);            
            },
        
            error:function(){
                console.log('error');
            }
        });
    }

    $(document).ready(function(){
        myVar = setInterval("telehealthCount()", 30000);
    });
</script>

<hr class="mt-5">
<h3 class="telehealth-count-text my-5">Over <span class="telehealth-count" id="telehealthCount"><?php echo $count?></span> Telehealth Appointments Today</h3>