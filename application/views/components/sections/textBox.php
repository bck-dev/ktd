<div class="<?php if($pageName=="Telehealth"): echo'yellow_background'; endif; ?>">
    <div class="container py-5 text_box_container">
        <div class="row">
            <?php foreach($textBoxes as $t): ?>
                <div class="col-lg-4 p-0 py-4 pr-3 text_box">
                    <?php if($t->link): ?><a href="<?php echo base_url(); ?><?php echo $t->link; ?>" class=""><?php endif; ?>
                        <p class="font20 b600 purple">
                            <?php echo $t->title; ?>
                        </p>
                    <?php if($t->link): ?></a><?php endif; ?>
                    <p class="font15">
                        <?php echo $t->content; ?>
                    </p>
                    <?php if($t->link): ?><a href="<?php echo base_url(); ?><?php echo $t->link; ?>" class="font16 purple b600">Discover more</a><?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>