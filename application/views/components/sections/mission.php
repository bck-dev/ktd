<div class="container">
    <div class="row pt-6 pb-4 white">
        <div class="col-lg-6 d-none d-sm-block">
            <div class="purple_background">
                <div class="p-5" style="background-image:url(<?php echo base_url('/assets/images/mission.png') ?>); background-repeat: no-repeat; background-position: top right; ">
                    <h3 class="white font26">Our Mission is to:</h3>
                    <ul class="font15 b600 px-3" style="list-style-image:url(<?php echo base_url('/assets/images/bullet.png') ?>);">
                        <li>Reduce infant and child mortality rates</li>
                        <li>Control infectious diseases</li>
                        <li>Foster healthy lifestyles</li>
                        <li>Improve quality of life for children with chronic conditions</li>
                    </ul>
                </div>
            </div>          
        </div>
        <div class="col-lg-6 d-block d-sm-none">
            <div class="purple_background">
                <div class="p-3 pr-5" style="background-image:url(<?php echo base_url('/assets/images/missionsm.png') ?>); background-repeat: no-repeat; background-position: top right; ">
                    <h3 class="white font26">Our Mission is to:</h3>
                    <ul class="font15 b600 px-3" style="list-style-image:url(<?php echo base_url('/assets/images/bullet.png') ?>);">
                        <li>Reduce infant and child mortality rates</li>
                        <li>Control infectious diseases</li>
                        <li>Foster healthy lifestyles</li>
                        <li>Improve quality of life for children with chronic conditions</li>
                    </ul>
                </div>
            </div>          
        </div>
        <div class="col-lg-6 d-none d-sm-block">
            <div class="light_blue_background">
                <div class="p-5" style="background-image:url(<?php echo base_url('/assets/images/vision.png') ?>); background-repeat: no-repeat; background-position: top right; ">
                    <h3 class="white font26">Our Vision is to:</h3>
                    
                    <p class="font15 b600 w-75">
                        Our pediatricians are dedicated to providing your family with the highest degree of health care. 
                        We operate under a simple, yet effective philosophy: 
                        Children are our future, and the preservation of their health is our duty.
                    </p>
                </div>
            </div>          
        </div>
        <div class="col-lg-6 d-block d-sm-none">
            <div class="light_blue_background">
                <div class="p-3 pr-4" style="background-image:url(<?php echo base_url('/assets/images/visionsm.png') ?>); background-repeat: no-repeat; background-position: top right; ">
                    <h3 class="white font26">Our Vision is to:</h3>
                    
                    <p class="font15 b600 w-75">
                        Our pediatricians are dedicated to providing your family with the highest degree of health care. 
                        We operate under a simple, yet effective philosophy: 
                        Children are our future, and the preservation of their health is our duty.
                    </p>
                </div>
            </div>          
        </div>
    </div>
</div>