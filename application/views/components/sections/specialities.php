<div class="yellow_background">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 p-0">
                <?php $this->load->view('components/sections/accordion'); ?>                  
            </div>

            <div class="col-lg-4 p-0">
                <?php $this->load->view('components/sections/featuredBox'); ?>
            </div>

        </div>
    </div>
</div>