<div class="beige_background">
    <div class="container p-5 text-center font26">
        <hr class="mt-5"/>
        <p class="beige_background logoSliderHeader dark_grey d-none d-sm-block">Our Partner Hospitals</p>        
        <p class="beige_background logoSliderHeaderMobile dark_grey d-block d-sm-none">Our Partner Hospitals</p>
        <section class="hospital-logos slider py-3 mt-5">
            <?php foreach($logos as $l): ?>
                <div class="slide"><img src="<?php echo base_url(''); ?>uploads/<?php echo $l->image?>"></div>
            <?php endforeach; ?> 
        </section>
    </div>
</div>