<p class="font15 b700 mt-3 mb-4">Check Eligibility</p>

<div>
    <p class="font13 b600 light_blue mb-2">STEP 01</p>
    <p class="font15 b600 mb-3">Choose Your Insurance Type</p>

    <select class="form-control mb-5 custom_feild" id="itype">
        <option value="" disabled="disabled" selected="selected">Choose Your Insurance Type</option>
        <option value="it1">I Have Medi_cal HMO/IPA</option>
        <option value="it2">I Have Commercial HMO/IPA</option>
        <option value="it3">I Have PPO/POS insurance</option>
        <option value="it5">I Have Regal, Lakeside & Healthcare partners</option>
        <option value="it4">I'm Not Sure</option>
    </select>
</div>

<button class="btn btn-block insurance_btn mb-5" id="ibtn">View Details</button>

<p class="font26 b600">Insurances Accepted</p>
<p class="font20 insurance_form_content">
    We accept most insurance including HMOs, PPOs, and Medi-Cal. If you are a new patient and would like to verify if we accept your insurance, please email our new patient liaison: <a href="mailto:newpatient@ktdoctor.com">newpatient@ktdoctor.com.</a>
    <br /><br />
    If your insurance card list one of our office addresses or one of our physicians, you could still be seen at any of our offices or see any one of our physicians.
    <br /><br />
    We Welcome all  Commercial and Medi-Cal HMO members with Regal, Lakeside, Health Care Partners and Eastland Members at Kids and Teens
    
</p>


<script>
    $('#itype').change(function(){
        $('#ibox').css("max-height", "1150px");
        $('#ibtn').css("display", "block");
    });

    $('#ibtn').click(function(){
        var itype = $('#itype').val();
        if(itype=="it4"){
            var title = "Insurance Instructions";
            var content ="We accept most insurance, including PPO, HMO, MediCal and CHDP. So, if you’re not sure what type of insurance you have, or didn’t see your health plan in our list of options, chances are we can still help.";
        }
        else if(itype=="it1"){
            var title = "Medi_cal HMO/IPA";
            var content ="Serendib-Molina IPA + our physicians (Preferred) <br /><br />  La care Exceptional Care Serendib IPA + our physicians (Preferred) <br /><br />  Healthnet Exceptional care – Serendib + our physicians (Preferred) <br /><br />  Anthem Blue cross- Exceptional care – Serendib + our physicians (Preferred)";
        }
        else if(itype=="it2"){
            var title = "Commercial HMO/IPA";
            var content ="Exceptional care Blue Sheild commercial(Preferred) <br /><br />  Exceptional care Blue Sheild Plus (Preferred) <br /><br />  Exceptional care Cigna Commercial (Preferred) <br /><br />  Exceptional care Healthnet Salud con Commercial (Preferred)";       
        }

        else if(itype=="it3"){
            var title = "PPO/POS insurance";
            var content = "Aetna PPO/POS and Managed Choice <br /><br />  Anthem Blue Cross of CA PPO/POS <br /><br />  Blue Shield of CA PPO/POS <br /><br />  BlueCard Plans/POS <br /><br />  Health Net PPO/POS <br /><br />  Cigna PPO Global and Open Access Plus and POS <br /><br />  United Health Care PPO/POS <br /><br /> OSCAR PPO <br /><br />  Keenan-Prime Healthcare <br /><br />  Tri Care <br /><br />  And most all PPO insurance programs";   
        }
        
        else if(itype=="it5"){
            var title = "Regal, Lakeside & Healthcare Partners";
            var content = "We Welcome all Commercial and Medi-cal HMO Members with Regal, Lakeside, Health Care Partners and Eastland Members at Kids and Teens";   
        }
        
        $('#imessagetitle').text(title);
        $('#imessagecontent').html(content);
        $('#imessagetitlemob').text(title);
        $('#imessagecontentmob').html(content);
        
        $('html, body').animate({
            scrollTop: $("#imessage").offset().top-300
        }, 1000);
 
       
    });

</script>