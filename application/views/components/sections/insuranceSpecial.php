<div class="featured_box yellow_background insurance_special_box">
    <div class="w-100">
        <div class="p-5 insurance_special_box_head">
            <p class="font20 b600">Make an Appointment instantly</p>
            <div class="font18">Simply choose one of the options below to continue.</div>
        </div>

        <div class="w-100 white_background insurance_special_box_body">
            <div class="row insurance_text p-2 px-3">
                <div class="col-1 align-text-bottom purple font20 p-0 pt-3"><i class="far fa-comments"></i></div>
                <div class="col-6 align-text-bottom purple font13 p-0 pl-2">ENGLISH TEXT <br /><span class="font18">(626) 298-7121</span></div>
                <div class="col-5 align-self-center p-0 pt-1" ><a href="sms:6262987121" class="it_btn btn-block btn-block b600"><i class="far fa-comments"></i> &nbsp; Text Us</a></div>
            </div>

            <div class="row insurance_text p-2 px-3">
                <div class="col-1 align-text-bottom purple font20 p-0 pt-3"><i class="far fa-comments"></i></div>
                <div class="col-6 align-text-bottom purple font13 p-0 pl-2">SPANISH TEXT <br /><span class="font18">(626) 269-7744</span></div>
                <div class="col-5 align-self-center p-0 pt-1" ><a href="sms:6262697744" class="it_btn btn-block b600"><i class="far fa-comments"></i> &nbsp; Text Us</a></div>
            </div>

            <div class="w-100 insurance_text py-3 px-0 ibt mt-4">
                <a href="<?php echo base_url('doctors'); ?>"><div class="align-text-bottom purple font18 b600 p-0"><i class="fas fa-search pr-3"></i> Find a Doctor</div></a>
            </div>

            <div class="w-100 insurance_text py-3 px-0 ibt">
                <a href="<?php echo base_url('appointment'); ?>"><div class="align-text-bottom purple font18 b600"><i class="far fa-calendar-check pr-3"></i> Online Appointment</div></a>
            </div>
        </div>
    </div>
</div>