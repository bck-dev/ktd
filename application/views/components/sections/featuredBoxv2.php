<div class="featured_box light_blue_background pb-2">
    <?php $i=1; foreach($featured as $f): ?>
        <?php if($i==1):?>
            <div class="w-100">
                <div class="p-5 featured_box_content">
                    <p class="white font20 b700 m-0">
                        <?php echo $f->title ?>
                    </p>
                    <div class="white font22 fcontent m-0">
                        <?php echo $f->content ?>
                    </div>
                </div>
                <?php if($f->button_text!=" "):?>
                    <div class="w-100 white_background p-4 text-center mb-3">
                        <a href="<?php echo base_url(); ?><?php echo $f->link ?>" class="light_blue font18"><?php echo $f->button_text ?></a>
                    </div>
                <?php endif; ?> 
            </div>
            
            
        <?php endif; ?>  
    <?php $i++; endforeach; ?>  
</div>