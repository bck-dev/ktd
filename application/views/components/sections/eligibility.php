<div class="yellow_background eligibility_box">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 p-0 pt-5 pb-5 ">
                <div class="d-none d-lg-block">
                    <div class="row">
                        <div class="col-lg-6 text-center p-2 pt-5 pr-5 pb-5 eligibility_box1">
                            <p class="font20 darker_blue b600">We accept most <br />Insurances</p>
                            <a href="<?php echo base_url('insurance'); ?>" class="eligibility_btn">Check Eligibility</a>
                        </div>
                        <div class="col-lg-6 text-center p-2 pt-5 pl-5 pb-5 eligibility_box2">
                            <p class="font20 darker_blue b600">Switching to our <br />Doctors</p>
                            <a href="<?php echo base_url('practice'); ?>" class="eligibility_btn">Check Eligibility</a>
                        </div>
                        <div class="col-lg-6 text-center  p-2 pt-5 pr-5 pb-5 eligibility_box3">
                            <p class="font20 darker_blue b600">Expectant <br />Mothers</p>
                            <a href="<?php echo base_url('expectantmothers'); ?>" class="eligibility_btn">Discover</a>
                        </div>
                        <div class="col-lg-6 text-center p-2 pt-5 pl-5 pb-5 eligibility_box4">
                            <p class="font20 darker_blue b600">Newborn <br />Care</p>
                            <a href="<?php echo base_url('newborncare'); ?>" class="eligibility_btn">Discover</a>
                        </div>
                    </div>
                </div>  
                <div class="d-block d-lg-none">
                    <div class="w-100"">
                        <div class="col-12 text-center p-4">
                            <p class="font20 darker_blue b600">We accept most <br />Insurances</p>
                            <a href="<?php echo base_url('insurance'); ?>" class="eligibility_btn b600">Check Eligibility</a>
                        </div>
                        <div class="col-12 text-center p-4">
                            <p class="font20 darker_blue b600">Switching to our <br />Doctors</p>
                            <a href="<?php echo base_url('practice'); ?>" class="eligibility_btn b600">Check Eligibility</a>
                        </div>
                        <div class="col-12 text-center p-4">
                            <p class="font20 darker_blue b600">Expectant <br />Mothers</p>
                            <a href="<?php echo base_url('expectantmothers'); ?>" class="eligibility_btn b600">Discover</a>
                        </div>
                        <div class="col-12 text-center p-4">
                            <p class="font20 darker_blue b600">Newborn <br />Care</p>
                            <a href="<?php echo base_url('newborncare'); ?>" class="eligibility_btn b600">Discover</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-1">  
            </div>
            
            <div class="col-lg-4 p-0">
                <div  class="purple_background p-0 pb-3 featured_box" style="margin-top: -50px;">
    
                    <div class="w-100">
                        <div class="p-5">
                            <div class="white font26">
                            Our board certified pediatricians are dedicated to providing your child with the highest degree of healthcare
                            </div>
                        </div>
                    </div> 
                </div>                     
            </div>
        </div>
    </div>
</div>