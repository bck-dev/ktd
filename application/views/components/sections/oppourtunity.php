<div class="container">
    <div class="row pt-7 oppourtunity_box">
        <div class="col-lg-6">
            <div class="purple_background p-4">
                <a href="<?php echo base_url(); ?>practice"><h3 class="white m-0 p-3">Physician Practice Opportunities <i class="fas fa-chevron-right ml-1"></i></h3></a>
            </div>
        </div>
        
        <div class="col-lg-6">
            <div class="light_blue_background p-4">
                <a href="<?php echo base_url(); ?>career"><h3 class="white m-0 p-3">Explore Career Opportunities <i class="fas fa-chevron-right ml-1"></i></h3></a>
            </div>          
        </div>

        <!-- <div class="col-lg-4">
            <div class="staff-access-bg p-4">
                <a href="https://login.microsoftonline.com/common/oauth2/authorize?client_id=4345a7b9-9a63-4910-a426-35363201d503&redirect_uri=https%3A%2F%2Fwww.office.com%2Flanding&response_type=code%20id_token&scope=openid%20profile&response_mode=form_post&nonce=637175224669892996.YmY2NDBhMzktYjUxNC00ZDkwLTlkNjgtM2Y4YmRjNGQyZDI1ZGM3NjI0OTAtMzE0NC00OTM1LWE2OWUtNzcyZWE0M2U1YzMx&ui_locales=en-US&mkt=en-US&client-request-id=1126c4cc-4135-4834-bddd-15ef21e48c93&state=vnpDxQn0wfMSMRP7TqJuKUMqVnr3dZR-mxVDmU1h6LVLQorzMKVtmx5Ct-qubs0vQ7vE4HCZrBLLhViuX-Sksrq5z3a3D0Ns8MLUC6MGxdW0dg5pApqPkpQxaMlBtBUgimK_3WIFrJnAfNPXlCBVw8w4_EhOT7iGgYx8t_WJWRl_-5QcDIkl_eKoI9eMSh-RYTp3JzdjqQMxD9OYwiqPEXw8bk07EjfvLmsQvtiIYWNimBcrhoDRUCeBekXQk78Xicn1CfVJrEa8zSTk2u24T2KWxPA4ApnuPfX-uVXYczIqGvZbmJDCHBuEjeayHejx1dBLXqBeqKD1i5Nqu3tuUTZTgqnU2y8IRxAGMYcR3MJELkdiVUCZNYw9ZcDiTHDKWEpwWvCnvREhSxi9lmj3YCBTF-0Cc1VjPmpWa4yN0em7KXpooxcV67_6ssEpkOfTTfNGkpNGHS5gMxjxwMuuGEC7NdALV_zonxRwA_4Pj8u_EQ5jMZbUwkSGzS_V9Fr_V_8bQNK9LSCEqFL92d94JikRmUs5Bv8s1saNEjYu-SgOJ_qO_VRbgqXjdB91smsDJGH-7YX-JA-Pz8nqzoVkdebgKacXG3tZvElNFnQc-1N28kyjNem35ErDbqtCKsSmci7gQ2rSBD9wPQTuz0tj7A&x-client-SKU=ID_NETSTANDARD2_0&x-client-ver=6.3.0.0&sso_reload=true" target="blank"><h3 class="white m-0 p-3">Staff <br/> Access<i class="fas fa-chevron-right ml-1"></i></h3></a>
            </div> -->          
        </div>
    </div>
</div>