<div class="pl-0 pr-5 accordion" id="accordion" >
    <?php $i=1; foreach($accordions as $accordion): ?>
        <div class="card p-0 trans border-bottom" style="border:#A9A9A9">
            <div class="pl-0 pb-2 pt-2" id="heading<?php echo $accordion->id ?>">
                <a href="" class="font20 purple b600 pl-0" data-toggle="collapse" data-target="#collapse<?php echo $accordion->id ?>" aria-expanded="true" aria-controls="collapse<?php echo $accordion->id ?>">
                    <?php echo $accordion->title ?>
                </a>
            </div>

            <div id="collapse<?php echo $accordion->id ?>" class="collapse <?php if ($i==1): ?>show<?php endif; ?>" aria-labelledby="heading<?php echo $accordion->id ?>" data-parent="#accordion">
                <div class="card-body pl-0 font15 dark_purple pb-2 pt-2">
                    <?php echo $accordion->content ?>
                </div>
            </div>
        </div>
    <?php $i++; endforeach; ?>              
</div>