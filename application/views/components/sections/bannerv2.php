<?php $i=1; foreach($banner as $b): ?>
    <?php if($i==1):?>
        <div class="w-100 p-0">
            <div class="w_custom other_hero d-none d-lg-block" style="background-image: url('<?php echo base_url();?>uploads/<?php echo $b->image?>'); "></div>
            <div class="w-100 p-0 d-none d-lg-block" style="position:absolute; top: 200px; ">
                <div class="container p-0" style="position: relative;">
                    <div class="col-lg-6 offset-lg-6 maroon_background p-5">
                        <p class="white font45 lh1">
                            <?php echo $b->title; ?>
                        </p>
                        <div class="white font15">
                            <?php echo $b->content?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="w_100 other_hero d-block d-sm-none" style="background-image: url('<?php echo base_url();?>uploads/<?php echo $b->image?>'); "></div> -->
            <div class="w-100 p-0 d-block d-lg-none">
                <div class="container p-0" style="position: relative;">
                    <div class="col-lg-6 offset-lg-6 maroon_background banner_inside">
                        <p class="white font45 mt-4">
                            <?php echo $b->title; ?>
                        </p>
                        <div class="white font15">
                            <?php echo $b->content?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php $i++; endif; ?>  
<?php $i++; endforeach; ?> 

