<?php $i=1; foreach($descriptionBox as $d): ?>
    <?php if($i==1):?>
        <div class="container">
            <div class="row py-5 patients-page-main-heading">
                <div class="col-lg-8 p-0 pt-3 pr-5 description_box">
                    <h1 class="light_blue"><?php echo $d->title; ?></h1>
                    <div class="mt-5 font18 lh2 patient-pages-content-area pb-0"><i><?php echo $d->content?></i></div>
                </div>
                <div class="col-lg-4 p-0 pt-3 description_box">
                    <img src="<?php echo base_url(''); ?>uploads/<?php echo $d->image?>" class="w-100 mt-3" />
                    <a href="<?php if(strpos($d->link, 'http') !== false): echo $d->link; else: echo base_url().$d->link; endif;?>">
                        <div class="p-3 yellow_background btn-custom font15 b600 text-center"><?php echo $d->button_text?></div>
                    </a>
                </div>
            </div>
        </div>
    <?php $i++; endif; ?>  
<?php $i++; endforeach; ?> 

