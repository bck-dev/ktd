<div class="container feature">
    <div class="row" style="margin-top: -100px;">
        <div class="col-lg-8 purple_background p-5 mobile_schedule">
            <div class="row align-self-bottom">
                <div class="col-12 col-lg-3"> 
                    <h5 class="white pb-3" style="min-width: 135px;">Our Locations</h5>
                </div>
                <div class="col-12 col-lg-9 white p-0 pl-2">
                    <p class="m-0"><span class="font12 b700">&nbsp; SORT BY:</span>
                    <div class="align-text-bottom text-left p-0">
                        <label for="opt1" class="radio">
                            <input type="radio" name="sort" id="opt1" class="hidden"/>
                            <span class="label"></span>Mon
                        </label>

                        <label for="opt5" class="radio">
                            <input type="radio" name="sort" id="opt5" class="hidden"/>
                            <span class="label"></span>Tue
                        </label>

                        <label for="opt6" class="radio">
                            <input type="radio" name="sort" id="opt6" class="hidden"/>
                            <span class="label"></span>Wed
                        </label>

                        <label for="opt7" class="radio">
                            <input type="radio" name="sort" id="opt7" class="hidden"/>
                            <span class="label"></span>Thu
                        </label>

                        <label for="opt8" class="radio">
                            <input type="radio" name="sort" id="opt8" class="hidden"/>
                            <span class="label"></span>Fri
                        </label>
                        
                        <label for="opt2" class="radio">
                            <input type="radio" name="sort" id="opt2" class="hidden"/>
                            <span class="label"></span>Sat
                        </label>
                        
                        <label for="opt3" class="radio">
                            <input type="radio" name="sort" id="opt3" class="hidden"/>
                            <span class="label"></span>Sun
                        </label>
                        
                        <label for="opt4" class="radio">
                            <input type="radio" name="sort" id="opt4" class="hidden"/>
                            <span class="label"></span>After Hours
                        </label>

                        </p>
                    
                    </div>
                </div>
            </div>
            <div class="d-none d-lg-block">
                <div class="row row-eq-height white mt-4">
                    <?php $i=1; foreach($locations as $location): ?>
                        <div class="col-lg-6 <?php if($i==10):?>mtm-6<?php endif; ?> <?php if($i==10): echo"order-3"; else: echo"order-1"; endif;?> home_location <?php if($x==5): echo"float-left"; endif;?>">
                            <div class="row <?php if($location->afterHour!="on"): ?>notAfterHour <?php endif; ?><?php if($location->saturday!="on"): ?>notSaturday <?php endif; ?><?php if($location->sunday!="on"): ?>notSunday <?php endif; ?><?php if($location->mon!="on"): ?>notMon <?php endif; ?><?php if($location->tue!="on"): ?>notTue <?php endif; ?><?php if($location->wed!="on"): ?>notWed <?php endif; ?><?php if($location->thu!="on"): ?>notThu <?php endif; ?><?php if($location->fri!="on"): ?>notFri <?php endif; ?> common" >
                                <div class="col-5 location_cell p-0 align-self-center font16 b600" ><a style="color: white" href="<?php echo base_url('locations/').str_replace(' ', '',strtolower($location->name));?>"><?php echo $location->name ?></a> <?php if($location->afterHour=="on"): ?><i class="far fa-clock yellow font10"></i>&nbsp;<?php endif; ?></div>
                                <div class="col-3 location_cell p-0 align-self-center lt_btn_box" ><a href="sms:6262987121" class="lt_btn btn-block b700"><i class="far fa-comments"></i> &nbsp; Text Us</a></div>
                                <div class="col-4 location_cell p-0 time b600 text-right align-self-center font12 currentDay" >
                                    <?php if($location->fromHour!="00:00:00"): ?>
                                        <!-- <?php if($location->afterHour=="on"): ?><i class="far fa-clock yellow font10"></i>&nbsp;<?php endif; ?>  -->
                                        <!-- <span class="b400">Hrs</span> -->
                                        <?php $date = DateTime::createFromFormat( 'H:i:s', $location->fromHour); echo $date->format( 'g:i a'); ?> 
                                        - <?php $date = DateTime::createFromFormat( 'H:i:s', $location->toHour); echo $date->format( 'g:i a'); ?>
                                    <?php else: ?> 
                                        <span>Closed</span>
                                    <?php endif; ?> 
                                </div>
                                <?php foreach($location->schedule as $ls): ?>
                                    <div class="col-4 location_cell p-0 time b600 text-right align-self-center font12 notCurrentDay d-none <?php echo $ls->day;?>">
                                        <?php if($ls->fromHour!="00:00:00"): ?>
                                            <!-- <?php if($location->afterHour=="on"): ?><i class="far fa-clock yellow font10"></i>&nbsp;<?php endif; ?>  -->
                                            <!-- <span class="b400">Hrs</span> -->
                                            <?php $date = DateTime::createFromFormat( 'H:i:s', $ls->fromHour); echo $date->format( 'g:i a'); ?> 
                                            - <?php $date = DateTime::createFromFormat( 'H:i:s', $ls->toHour); echo $date->format( 'g:i a'); ?>
                                        <?php else: ?> 
                                            <span>Closed</span>
                                        <?php endif; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php $i++; endforeach; ?>
                    <div class="col-lg-6 order-2 textline_location">   
                        <div class="row p-3 no_border" >
                            <div class="col-6 p-0 font13 text-center" >
                                <a href="sms:6262987121" class="lt_btn_big btn-block b600 font16"><i class="far fa-comments yellow"></i> &nbsp; (626) 298-7121</a>
                                ENGLISH TEXT
                            </div>
                            <div class="col-6 p-0 font13 text-center" >
                                <a href="sms:6262697744" class="lt_btn_big btn-block b600 font16"><i class="far fa-comments yellow"></i> &nbsp; (626) 269-7744</a>
                                SPANISH TEXT
                            </div>
                        </div>
                    </div>  
                </div>
            </div>

            <div class="d-block d-lg-none">
                <div class="row row-eq-height white mt-4">
                    <?php foreach($locations as $location): ?>
                        <div class="col-lg-6 home_location">
                            <div class="row <?php if($location->afterHour!="on"): ?>notAfterHour <?php endif; ?><?php if($location->saturday!="on"): ?>notSaturday <?php endif; ?><?php if($location->sunday!="on"): ?>notSunday <?php endif; ?><?php if($location->mon!="on"): ?>notMon <?php endif; ?><?php if($location->tue!="on"): ?>notTue <?php endif; ?><?php if($location->wed!="on"): ?>notWed <?php endif; ?><?php if($location->thu!="on"): ?>notThu <?php endif; ?><?php if($location->fri!="on"): ?>notFri <?php endif; ?> common" >
                                <div class="col-5 p-0 location_cell font16 b600 align-self-center" ><?php echo $location->name ?> <?php if($location->afterHour=="on"): ?><i class="far fa-clock yellow font12"></i>&nbsp;<?php endif; ?></div>
                                <div class="col-3 p-0 location_cell lt_btn_box align-self-center" ><a href="sms:6262987121" class="lt_btn_mobile btn-block b700"><i class="far fa-comments"></i> &nbsp; Text Us</a></div>
                                <div class="col-4 p-0 location_cell time pt-1 b600 text-right align-self-center font12 currentDay" >
                                    <?php if($location->fromHour!="00:00:00"): ?> 
                                        <!-- <span class="b400">Hrs</span> -->
                                        <?php $date = DateTime::createFromFormat( 'H:i:s', $location->fromHour); echo $date->format( 'g:i a'); ?> 
                                        - <?php $date = DateTime::createFromFormat( 'H:i:s', $location->toHour); echo $date->format( 'g:i a'); ?>
                                    <?php else: ?> 
                                        <span>Closed</span>
                                    <?php endif; ?> 
                                </div>
                                <?php foreach($location->schedule as $ls): ?>
                                    <div class="col-4 p-0 location_cell time b600 text-right align-self-center font12 notCurrentDay d-none <?php echo $ls->day;?>">
                                        <?php if($ls->fromHour!="00:00:00"): ?> 
                                            <!-- <span class="b400">Hrs</span> -->
                                            <?php $date = DateTime::createFromFormat( 'H:i:s', $ls->fromHour); echo $date->format( 'g:i a'); ?> 
                                            - <?php $date = DateTime::createFromFormat( 'H:i:s', $ls->toHour); echo $date->format( 'g:i a'); ?>
                                        <?php else: ?> 
                                            <span>Closed</span>
                                        <?php endif; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <div class="col-lg-6 order-2 textline_location">   
                        <div class="row p-3 no_border" >
                            <div class="col-6 p-0 font13 text-center" >
                                <a href="sms:6262987121" class="lt_btn_big btn-block b600 font16"><i class="far fa-comments yellow"></i> &nbsp; (626) 298-7121</a>
                                ENGLISH TEXT
                            </div>
                            <div class="col-6 p-0 font13 text-center" >
                                <a href="sms:6262697744" class="lt_btn_big btn-block b600 font16"><i class="far fa-comments yellow"></i> &nbsp; (626) 269-7744</a>
                                SPANISH TEXT
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
           
            
            <div class="row home_appoinment">
                <div class="col-lg-12 white font13 text-center pt-3">
                    Is your local office closed?
                    <span class="font16 ml-2">Call Our Hotline <a class="font18 yellow" href="tel:8183615437" onclick="ga('send', 'event', 'Text Line Tracking', 'Click to Message Home', '818-361-5437', 0);">(818) 361-5437</a></span>
                    <span class="font16 ml-2">or make an <a class="font18 yellow" href="<?php echo base_url('appointment'); ?>">APPOINTMENT</a></span>
                </div>

                <div class="darker_blue white_background w-100 text-center py-1 mt-2 blinking">
                    We are now offering 24/7 Telehealth appointments during COVID -19 pandemic.
                </div>
            </div>
        </div>

        <div class="col-lg-4 light_blue_background p-4">
                <div class="row w-100 pt-4 mt-3" >
                    <div class="col-6 p-0" >
                        <span class="font20 white b700" id="dname">Janesri De Silva</span>
                        <br />
                        <span class="font15 white b400" id="dqualification"><b>Medical Director</b> <br />Board Certified <br /> MD, FAAP</span>
                        <br />
                        <br />
                        <!--<span class="font15 white" id="dlocation">Beverly Hills</span>-->
                        
                        <a href="tel:8183615437"><span class="font15 white b700" id="dtelephone">(818) 361-5437</span></a>
                    </div>
                    <div class="col-6 p-0">
                        <img src="<?php echo base_url('assets/images/janasri-n.png'); ?>" class="hd_img d-block w-100" id="dimage" alt="">
                    </div>
                    <hr class="hd_hr mt-0" align="left"/>
                </div>
                
                <div class="row w-100 pt-3 mt-3 ml-2 pr-3" >
                    <div class="col-12 p-0 font15 text-center white">
                        All our doctors are <b>board certified</b> <br />doctors with over 10+ years experience
                        <br />
                        <br />
                        We have numerous physicians on staff at some of the most reputable hospitals in Los Angeles.
                        <br />
                        <br />
                        <a href="<?php echo base_url('doctors'); ?>" class="btn btn-block btn-custom b700">Discover More</a>
                        <a href="<?php echo base_url('appointment'); ?>" id="appointmentUrl" class="btn btn-block btn-custom b700 mt-2 purple_btn">Make An Appointment</a>
                        <!-- <a href="<?php echo base_url('appointment'); ?>/beverlyhills/10" id="appointmentUrl" class="btn btn-block btn-custom b700 mt-2 purple_btn">Make An Appointment</a> -->
                    </div>
                </div>            
            </div>
        </div>
    </div>
</div>



<script>

    $(document).ready(function(){
        if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
        } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
        }
    });

    function showPosition(position) {
        console.log(position.coords);
        $.ajax({
        url: '<?php echo base_url('web/api/homedoctor') ?>', 
        type:'get',
        data: {lat: position.coords.latitude, lng: position.coords.longitude},
        dataType: 'json',
        success: function(results){ 
            var location = results['dlocation'];
            var noSpaceLocation = location.replace(/\s/g, '');
            var lowerCaseLocation = noSpaceLocation.toLowerCase();
            var appointmentUrl = '<?php echo base_url('appointment') ?>' + '/' + lowerCaseLocation + '/' + results['dlocationId']; 

            $("#dimage").attr("src", results['dimage']);
            $("#dname").text(results['dname']);            
            $("#dqualification").text(results['dqualification']);
            $("#dlocation").text(results['dlocation']);            
            $("#dtelephone").text(results['dtelephone']);
            $("#appointmentUrl").prop("href", appointmentUrl);
            
        },
    
        error:function(){
            console.log('error');
        }
        });
    }
    $("#opt3").click(function(){
        if ($('#opt3').val() == 'on') {
            $(".currentDay").removeClass('d-block');
            $(".currentDay").addClass('d-none'); 
            $(".notCurrentDay").removeClass('d-block'); 
            $(".notCurrentDay").addClass('d-none');        
            $(".Sunday").removeClass('d-none');
            $(".Sunday").addClass('d-block'); 
            $(".common").css('opacity', '1');
            $(".notSunday").css('opacity', '0.5');
            $( ".common" ).find( ".lt_btn" ).css( "display", "block" );
            $( ".notSunday" ).find( ".lt_btn" ).css( "display", "none" );            
            $( ".common" ).find( ".lt_btn_mobile" ).css( "display", "block" );
            $( ".notSunday" ).find( ".lt_btn_mobile" ).css( "display", "none" );
        }
    }); 
    $("#opt2").click(function(){
        if ($('#opt2').val() == 'on') {       
            $(".currentDay").removeClass('d-block');
            $(".currentDay").addClass('d-none');
            $(".notCurrentDay").removeClass('d-block'); 
            $(".notCurrentDay").addClass('d-none');
            $(".Saturday").removeClass('d-none');
            $(".Saturday").addClass('d-block'); 
            $(".common").css('opacity', '1');
            $(".notSaturday").css('opacity', '0.5');
            $( ".common" ).find( ".lt_btn" ).css( "display", "block" );
            $( ".notSaturday" ).find( ".lt_btn" ).css( "display", "none" );            
            $( ".common" ).find( ".lt_btn_mobile" ).css( "display", "block" );
            $( ".notSaturday" ).find( ".lt_btn_mobile" ).css( "display", "none" );
        }
    }); 
    $("#opt4").click(function(){
        if ($('#opt4').val() == 'on') { 
            $(".currentDay").removeClass('d-block');
            $(".notCurrentDay").removeClass('d-block'); 
            $(".notCurrentDay").addClass('d-none');
            $(".currentDay").removeClass('d-none')
            $(".currentDay").addClass('d-block');
            $(".common").css('opacity', '1');
            $(".notAfterHour").css('opacity', '0.5');        
            $( ".common" ).find( ".lt_btn" ).css( "display", "block" );
            $( ".notAfterHour" ).find( ".lt_btn" ).css( "display", "none" );            
            $( ".common" ).find( ".lt_btn_mobile" ).css( "display", "block" );
            $( ".notAfterHour" ).find( ".lt_btn_mobile" ).css( "display", "none" );
        }
    }); 
    $("#opt1").click(function(){
        if ($('#opt1').val() == 'on') {
            $(".currentDay").removeClass('d-block');
            $(".currentDay").addClass('d-none'); 
            $(".notCurrentDay").removeClass('d-block'); 
            $(".notCurrentDay").addClass('d-none');        
            $(".Monday").removeClass('d-none');
            $(".Monday").addClass('d-block');     
            $(".common").css('opacity', '1');
            $(".notMon").css('opacity', '0.5');
            $( ".common" ).find( ".lt_btn" ).css( "display", "block" );
            $( ".notMon" ).find( ".lt_btn" ).css( "display", "none" );            
            $( ".common" ).find( ".lt_btn_mobile" ).css( "display", "block" );
            $( ".notMon" ).find( ".lt_btn_mobile" ).css( "display", "none" );
        }
    }); 
    $("#opt5").click(function(){
        if ($('#opt5').val() == 'on') { 
            $(".currentDay").removeClass('d-block');
            $(".currentDay").addClass('d-none'); 
            $(".notCurrentDay").removeClass('d-block'); 
            $(".notCurrentDay").addClass('d-none');        
            $(".Tuesday").removeClass('d-none');
            $(".Tuesday").addClass('d-block');        
            $(".common").css('opacity', '1');
            $(".notTue").css('opacity', '0.5');
            $( ".common" ).find( ".lt_btn" ).css( "display", "block" );
            $( ".notTue" ).find( ".lt_btn" ).css( "display", "none" );            
            $( ".common" ).find( ".lt_btn_mobile" ).css( "display", "block" );
            $( ".notTue" ).find( ".lt_btn_mobile" ).css( "display", "none" );
        }
    }); 
    $("#opt6").click(function(){
        if ($('#opt6').val() == 'on') { 
            $(".currentDay").removeClass('d-block');
            $(".currentDay").addClass('d-none'); 
            $(".notCurrentDay").removeClass('d-block'); 
            $(".notCurrentDay").addClass('d-none');        
            $(".Wednesday").removeClass('d-none');
            $(".Wednesday").addClass('d-block');        
            $(".common").css('opacity', '1');
            $(".notWed").css('opacity', '0.5');
            $( ".common" ).find( ".lt_btn" ).css( "display", "block" );
            $( ".notWed" ).find( ".lt_btn" ).css( "display", "none" );            
            $( ".common" ).find( ".lt_btn_mobile" ).css( "display", "block" );
            $( ".notWed" ).find( ".lt_btn_mobile" ).css( "display", "none" );
        }
    }); 
    $("#opt7").click(function(){
        if ($('#opt7').val() == 'on') { 
            $(".currentDay").removeClass('d-block');
            $(".currentDay").addClass('d-none'); 
            $(".notCurrentDay").removeClass('d-block'); 
            $(".notCurrentDay").addClass('d-none');        
            $(".Thursday").removeClass('d-none');
            $(".Thursday").addClass('d-block');        
            $(".common").css('opacity', '1');
            $(".notThu").css('opacity', '0.5');
            $( ".common" ).find( ".lt_btn" ).css( "display", "block" );
            $( ".notThu" ).find( ".lt_btn" ).css( "display", "none" );            
            $( ".common" ).find( ".lt_btn_mobile" ).css( "display", "block" );
            $( ".notThu" ).find( ".lt_btn_mobile" ).css( "display", "none" );
        }
    }); 
    $("#opt8").click(function(){
        if ($('#opt8').val() == 'on') {
            $(".currentDay").removeClass('d-block');
            $(".currentDay").addClass('d-none'); 
            $(".notCurrentDay").removeClass('d-block'); 
            $(".notCurrentDay").addClass('d-none');        
            $(".Friday").removeClass('d-none');
            $(".Firday").addClass('d-block');         
            $(".common").css('opacity', '1');
            $(".notFri").css('opacity', '0.5');
            $( ".common" ).find( ".lt_btn" ).css( "display", "block" );
            $( ".notFri" ).find( ".lt_btn" ).css( "display", "none" );            
            $( ".common" ).find( ".lt_btn_mobile" ).css( "display", "block" );
            $( ".notFri" ).find( ".lt_btn_mobile" ).css( "display", "none" );
        }
    }); 
</script>