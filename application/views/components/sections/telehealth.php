<div class="featured_box purple_background">
    <div class="w-100 pb-4">
        <div class="pr-3 pl-3 pt-3 pb-0">
            <div class="row">
                <div class="col-md-6 telehealth-feature-text white textus_box_content">24/7 Telehealth Appointments</div>
                <div class="col-md-6">
                    <div class="telehealth-feature-img">
                        <img src="<?php echo base_url();?>assets/images/telehealth.png" class="w-100" style="box-shadow: 5px 10px #888888;"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="w-100 white_background p-0 pl-5 py-4" style="z-index: 1000;">
                <div class="d-flex insurance_text p-0">
                    <div class="align-text-bottom purple font20 pr-2 pt-3"><i class="far fa-comments"></i></div>
                    <div class="align-text-bottom purple font13 pr-3">ENGLISH <br /><span class="font18">(626) 298-7121</span></div>
                    <div class="align-self-center p-0 pt-1" ><a href="sms:6262987121" class="it_btn btn-block b600"><i class="far fa-comments"></i> &nbsp; Text Us</a></div>
                </div>

                <div class="d-flex insurance_text p-0 mt-3">
                    <div class="align-text-bottom purple font20 pr-2 pt-3"><i class="far fa-comments"></i></div>
                    <div class="align-text-bottom purple font13 pr-3">SPANISH <br /><span class="font18">(626) 269-7744</span></div>
                    <div class="align-self-center p-0 pt-1" ><a href="sms:6262697744" class="it_btn btn-block b600"><i class="far fa-comments"></i> &nbsp; Text Us</a></div>
                </div>                
            <a href="" class=""></a>
        </div>
    </div>
</div>