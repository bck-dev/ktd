<div id="imessage">
    <div class="p-0">
        <p class="font26 b600 white" id="imessagetitle">Insurance Instructions</p>
        <p class="font18 white" id="imessagecontent">
            We accept most insurance, including PPO, HMO, MediCal and CHDP. So, if you’re not sure what type of insurance 
            you have, or didn’t see your health plan in our list of options, chances are we can still help.
        </p>        
        <p class="font18 white b600">
            If you belong to another HMO/IPA or have another office listed; you can still come into our office and have it all changed to one of our office and physicians.
            <br />Or contact us on <a href="tel:8183615437" class="white">(818) 361-5437</a> and have it changed over the phone.                
        </p>
    </div>
</div>