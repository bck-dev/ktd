<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<?php $this->load->view('components/sections/banner'); ?>

<div class="accordion-body pb-7" style="background-color: white;">
  <h1 class="text-center font-weight-bold pt-4 pb-4" >Frequently Asked Questions</h1>
  <section class="container px-0 pt-3" id="how-to">  
    <div class="panel-group" id="superaccordion">
      <?php $i=1; foreach($accordions as $accordion): ?>
        <div class="panel">
        <div class="panel-heading parent">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#superaccordion" href="#collapse<?php echo $accordion->id; ?>" aria-expanded="<?php if($i==1): echo "true"; else: echo "false"; endif; ?>">
              <h3 class="font24 dark_purple"><?php echo $accordion->title; ?></h3>
            </a>
        </div>
        <div id="collapse<?php echo $accordion->id; ?>" class="panel-collapse collapse <?php if($i==1): echo "show"; endif; ?>">
          <div class="panel-body">
            <div class="panel-group" id="accordion<?php echo $accordion->id; ?>">
              <div class="inside-body" style="background-color: #efefef;">
                <?php echo $accordion->content; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php $i++; endforeach; ?>  
    </div>       
  </section>
</div>


<?php $this->load->view('components/common/footer'); ?>

