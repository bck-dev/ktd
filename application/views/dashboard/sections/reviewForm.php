<div class="row">
    <div class="col-6" .mt-2>
      <div class="content-wrapper p-3">
 
      <?php $this->load->view('dashboard/sections/error') ?>
 
     <!-- Main content -->
     <section class="content">
       <div class="container-fluid">
         <div class="row">
           <!-- left column -->
           <div class="col-lg-12">
             <!-- general form elements -->
             <div class="card card-primary">
               <div class="card-header">
                 <h3 class="card-title">Reviews Form</h3>
               </div>
               <!-- /.card-header -->
               <!-- form start -->
               <form action="<?php echo base_url('admin/reviews/'.$action.'/');?><?php echo $updateData->id ?>" method="POST" name="reviewsForm" >
                    <div class="card-body">
                        <div class="form-group">
                            <label for="inputName">Name</label>
                            <input type="text" class="form-control" placeholder="Enter Name" name='name' value="<?php echo $updateData->name; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="inputTitle">Title</label>
                            <input type="text" class="form-control" placeholder="Enter Title" name='title' value="<?php echo $updateData->title; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="inputRating">Rating</label>
                            <input type="number"  placeholder="1.00" step="0.5" min="1" max="5" class="form-control" placeholder="Enter Rating" name='rating' value="<?php echo $updateData->rating; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="inputReview">Review</label>
                            <textarea class="form-control textarea" rows="10" placeholder="Enter Review" name='review' required><?php echo $updateData->review; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="selectType">Type</label>                  
                            <select class="custom-select" name='type' id="type" required>
                                <?php if($action=="update"): ?>

                                    <option value="<?php echo $updateData->type; ?>"><?php echo $updateData->type; ?></option>
                                <?php else: ?>
                                    <option value="" disabled="disabled" selected="selected">Select Type</option>
                                <?php endif; ?>
                                <option value="Doctor">Doctor</option>
                                <option value="Location">Location</option>
                                <option value="Service">Service</option>
                            </select> 
                        </div> 

                        <div <?php if($action=='add'): ?>class="form-group d-none" <?php elseif($action=='update' AND $updateData->type!=='Location'): ?>class="form-group d-none" <?php endif; ?> id="locationBox" > 
                            <label for="selectLocation">Location</label>                  
                            <select class="custom-select" name='location'  id="location">                            
                               <?php if($action=="update"): ?>                                                     
                                    <option value="<?php echo $updateData->locationId; ?>"><?php echo $updateData->lname; ?></option>
                                <?php else: ?>
                                    <option value="" disabled="disabled" selected="selected">Select Location</option>
                                <?php endif; ?>
                                <?php foreach($locations as $location): ?>
                                    <option value="<?php echo $location->id; ?>"><?php echo $location->name; ?></option>
                                <?php endforeach; ?>    
                            </select>                        
                        </div>

                        <div <?php if($action=='add'): ?>class="form-group d-none" <?php elseif($action=='update' AND $updateData->type!=='Doctor'): ?>class="form-group d-none" <?php endif; ?>id="doctorBox">
                            <label for="selectDoctor">Doctor</label>                  
                            <select class="custom-select" name='doctor' id="doctor">
                                <?php if($action=="update"): ?>
                                    <option value="<?php echo $updateData->doctorId; ?>"><?php echo $updateData->dname; ?></option>
                                <?php else: ?>
                                    <option value="" disabled="disabled" selected="selected">Select Doctor</option>
                                <?php endif; ?>
                                <?php foreach($doctors as $doctor): ?>
                                    <option value="<?php echo $doctor->id; ?>"><?php echo $doctor->name; ?></option>
                                <?php endforeach; ?>    
                            </select>                                     
                        </div> 
                                        
                    </div>
                    <!-- /.card-body -->                              
                    <?php  if($action == 'update'): ?>
                        <div class="card-footer">
                            <button type="submit"  class="btn btn-primary btn-lg btn-block"  name="update">Update</button>
                        </div>
                    <?php else: ?>
                        <div class="card-footer">
                            <button type="submit"  class="btn btn-primary btn-lg btn-block" name="submit">Add</button>
                        </div>
                    <?php endif; ?>                           
                </form>
             </div>
          </div>           
      </section>
    </div>
 </div>


 <script>
     
    $('#type').change(function()
        {     
            if($('#type').val()=="Doctor"){
                $('#doctorBox').removeClass('d-none');
                $('#doctorBox').addClass('d-block');
                $('#doctor').prop('required',true);
            }
            else if($('#type').val()=="Location"){
                $('#locationBox').removeClass('d-none');
                $('#locationBox').addClass('d-block');
                $('#location').prop('required',true);
            }
            else{
                $('#doctorBox').removeClass('d-block');
                $('#doctorBox').addClass('d-none');
                $('#doctor').prop('required',false);
                $('#locationBox').removeClass('d-block');
                $('#locationBox').addClass('d-none');
                $('#location').prop('required',false);
             }       
        }                   
    );

</script>


 
