<div class="col-6 p-3">

<table id="datatable" class="table table-bordered table-striped">
          <thead>
          <tr>
              <th>Logo Order</th>
              <th>Image</th>
              <th>Page</th>
              <th>Action</th>             
          </tr>
          </thead>
          <tbody>
              <?php foreach($logoSliderData as $dataRow): ?>
                  <tr>
                      <td><?php echo $dataRow->logo_order; ?></td>
                      <td><img src="<?php echo base_url(); ?>/uploads/<?php echo $dataRow->image; ?>" width="100"/></td>
                      <td><?php echo $dataRow->name ?></td>
                       <td>
                          <a class="btn btn-xs btn-warning" href="<?php echo base_url('admin/logoslider/loadupdate/');?><?php echo $dataRow->id ?>">Edit</a>
                          <a class="btn btn-xs btn-danger" href="<?php echo base_url('admin/logoslider/delete/');?><?php echo $dataRow->id ?>">Delete</a>
                      </td>
                  </tr>
                
              <?php endforeach; ?>
          </tfoot>
          </table>

</div>
</div>
