<div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Highlighted Box Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modaltext">
                <div class="row">
                    <div class="col-md-4"><b>Title</b></div>
                    <div class="col-md-8 ml-auto"><p id="title"></p></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Color</b></div>
                    <div class="col-md-8 ml-auto"><p id="color"></p></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Page</b></div>
                    <div class="col-md-8 ml-auto"><p id="pages"></p></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-4"><b>Content</b></div>
                </div>
                <div class="row">
                    <div class="col-md-12"><p id="content" ></p></div>                           
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> 



<script>
    function getData(id){
        var boxId = $(id).val();
            
        $.ajax({
            url: '<?php echo base_url('api/highlightedbox') ?>', 
            type:'post',
            data: {id: boxId},
            dataType: 'json',
            success: function(results){ 

                $('#title').text(results['title']);
                $('#color').text(results['color']);
                $('#content').html(results['content']);
                            
                var pages =""; 
                jQuery.each(results['pages'], function( key, val ) {
                    pages = pages + "<p>" + val['name'] + "</p>";
                });
                $('#pages').html(pages);
            },
        
            error:function(){
                console.log('error');
            }
        });
    }
</script>

     