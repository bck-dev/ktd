<div class="row">
    <div class="col-6" .mt-2>
      <div class="content-wrapper p-3">
 
    <?php $this->load->view('dashboard/sections/error') ?>
 
     <!-- Main content -->
     <section class="content">
       <div class="container-fluid">
         <div class="row">
           <!-- left column -->
           <div class="col-lg-12">
             <!-- general form elements -->
             <div class="card card-primary">
               <div class="card-header">
                 <h3 class="card-title">Insurance Form</h3>
               </div>
               <!-- /.card-header -->
               <!-- form start -->
               <form action="<?php echo base_url('admin/insurance/'.$action.'/');?><?php echo $updateData->id ?>" method="POST" name="insuranceForm" >
                 <div class="card-body">
                   <div class="form-group">
                          <label for="inputName">Insurance Name</label>
                          <input type="text" id="inputName" class="form-control" placeholder="Enter name" name='insuranceName' value="<?php echo $updateData->insuranceName; ?>" required>
                        </div>                
                </div>
                               <!-- /.card-body -->                               
              <?php  if($action == 'update') { ?>
                <div class="card-footer">
                  <button type="submit"  class="btn btn-primary btn-lg btn-block" name="update">Update</button>
                </div>
              <?php }else { ?>      
              <div class="card-footer">
                  <button type="submit"  class="btn btn-primary btn-lg btn-block" name="submit">Add</button>
                </div>
              <?php } ?>                         
                  </form>
             </div>
          </div>           <!-- /.card -->  <!-- Form Element sizes -->
      </section>
    </div>
 </div>
