<div class="col-6 p-3">

<div class="row">
<div class="col-sm">
    <a class="btn btn-sm btn-primary float-right" href="<?php echo base_url('admin/insurancelocation');?>">Add Location to Insurance</a>
</div>

</div>

<table id="datatable" class="table table-bordered table-striped">
          <thead>
          <tr>
              <th>Insurance Name</th>
              <th>Action</th>             
          </tr>
          </thead>
          <tbody>
              <?php foreach($insuranceData as $dataRow): ?>
                  <tr>
                      <td><?php echo $dataRow->insuranceName; ?></td>
                       <td>
                          <a class="btn btn-xs btn-warning" href="<?php echo base_url('admin/insurance/loadupdate/');?><?php echo $dataRow->id ?>">Edit</a>
                          <a class="btn btn-xs btn-danger" href="<?php echo base_url('admin/insurance/delete/');?><?php echo $dataRow->id ?>">Delete</a>
                      </td>
                  </tr>
                
              <?php endforeach; ?>
          </tfoot>
          </table>

</div>
</div>
