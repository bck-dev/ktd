<div class="row">
    <div class="col-6" .mt-2>
      <div class="content-wrapper p-3">
 
      <?php $this->load->view('dashboard/sections/error') ?>
 
     <!-- Main content -->
     <section class="content">
       <div class="container-fluid">
         <div class="row">
           <!-- left column -->
           <div class="col-lg-12">
             <!-- general form elements -->
             <div class="card card-primary">
               <div class="card-header">
                 <h3 class="card-title">Location-Insurance Form</h3>
               </div>
               <!-- /.card-header -->
               <!-- form start -->
               <form action="<?php echo base_url('admin/insurancelocation/add');?><?php echo $updateData->id ?>" method="POST" name="HighlightedBoxForm" enctype='multipart/form-data' >
                 <div class="card-body">
                  <div class="form-group">
                      <div class="col-sm-12">
                          <label for="selectPage">Location</label>                  
                              <select class="custom-select" name='locationId' required>
                                <?php if($action=="update"): ?>
                                  <option value="<?php echo $updateData->color; ?>"><?php echo $updateData->locationId; ?></option>
                                <?php else: ?>
                                  <option value="" disabled="disabled" selected="selected">Select Location</option>
                                <?php endif; ?>
                                <?php foreach($locations as $location): ?>
                                  <option value="<?php echo $location->id; ?>"><?php echo $location->name; ?></option>
                                <?php endforeach; ?> 
                                 </select> 
                          </div>                         
                     </div>  

                  <div class="form-group">
                      <div class="col-sm-12">
                          <label for="selectPage">Insurance</label>                  
                              <select class="custom-select" name='insuranceId' required>
                                <?php if($action=="update"): ?>
                                  <option value="<?php echo $selectedPage->insuranceId; ?>"><?php echo $selectedPage->insuranceName; ?></option>
                                <?php else: ?>
                                  <option value="" disabled="disabled" selected="selected">Select Insurance</option>
                                <?php endif; ?>
                                <?php foreach($insurances as $insurance): ?>
                                  <option value="<?php echo $insurance->id; ?>"><?php echo $insurance->insuranceName; ?></option>
                                <?php endforeach; ?>    
                              </select> 
                          </div>                         
                     </div>                      
                </div>
                               <!-- /.card-body -->                              
                               <?php  if($action == 'update') { ?>
                              <div class="card-footer">
                                <button type="submit"  class="btn btn-primary btn-lg btn-block" name="update">Update</button>
                              </div>
                            <?php }else { ?>
                                  
                              <div class="card-footer">
                                <button type="submit"  class="btn btn-primary btn-lg btn-block" name="submit">Add</button>
                              </div>
                            <?php } ?>
                            
                  </form>
             </div>
          </div>           <!-- /.card -->  <!-- Form Element sizes -->
      </section>
    </div>
 </div>
