<div class="col-6 p-3">

<table id="datatable" class="table table-bordered table-striped">
          <thead>
          <tr>
              <th>Location</th>
              <th>Insurance</th>
              <th>Action</th>             
          </tr>
          </thead>
          <tbody>
              <?php foreach($locationData as $dataRow): ?>
                  <tr>
                      <td><?php echo $dataRow->name ?></td>    
                      <td><?php echo $dataRow->insuranceName ?></td>
                       <td>
                          <a class="btn btn-xs btn-warning" href="<?php echo base_url('admin/highlightedbox/loadupdate/');?><?php echo $dataRow->id ?>">Edit</a>
                          <a class="btn btn-xs btn-danger" href="<?php echo base_url('admin/highlightedbox/delete/');?><?php echo $dataRow->id ?>">Delete</a>
                      </td>
                  </tr>
        
                  <?php endforeach; ?>   
          </tfoot>
          </table>

</div>
</div>
