<?php $this->load->view('dashboard/common/header.php')?>

<?php $this->load->view('dashboard/common/sidebar.php')?>

<div class="content-wrapper p-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Doctors <a href="<?php echo base_url('admin/doctor/newdoctor');?>" class="btn btn-sm btn-success ml-4">Add Doctor</a> </h3>
        </div>
        <div class="card-body">
            <table id="datatable" class="table table-bordered table-striped">
            <thead>
            <tr>         
                <th>Image</th>
                <th>Name</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                <?php foreach($doctorData as $dataRow): ?>
                    <tr>
                        <td><img src="<?php echo base_url(); ?>/uploads/<?php echo $dataRow->image; ?>" width="70"/></td>
                        <td><b><?php echo $dataRow->name; ?></b><br /><?php echo $dataRow->qualification; ?></td>
                        <td>
                              
                            <button class="btn btn-xs btn-info" onclick="getData(this);" id="btn" data-toggle="modal" data-target="#exampleModalScrollable" value="<?php echo $dataRow->id ?>">View More</button>
                            <a class="btn btn-xs btn-warning" href="<?php echo base_url();?>admin/doctor/loadupdate/<?php echo $dataRow->id ?>">Edit</a>
                            <a class="btn btn-xs btn-danger" href="<?php echo base_url();?>admin/doctor/delete/<?php echo $dataRow->id ?>">Delete</a>
                        
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tfoot>
            </table>
        </div>
    </div>
</div>

<?php $this->load->view('dashboard/sections/doctorModal.php')?>

<?php $this->load->view('dashboard/common/footer.php')?>

                                        