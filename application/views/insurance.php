<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<?php $this->load->view('components/sections/bannerv2'); ?>

<div class="container">
    <div class="row">
        <div class="col-lg-8 pr-5 pl-3 py-5 ibox mobile_correction_left" id="ibox">
            <?php $this->load->view('components/sections/insuranceForm'); ?>
        </div>
        <div class="col-lg-4 p-0 pt-5 mt-5">
            <?php $this->load->view('components/sections/featuredBoxv2'); ?>
        </div>
    </div>
</div>

<div class="purple_background" style="min-height: 400px;">
    <div class="container">
        <div class="row p-0">
            <div class="col-lg-8 py-5 pr-5 pl-0 mb-5 insurance_output">
                <?php $this->load->view('components/sections/insuranceOutput'); ?>
            </div>
            <div class="col-lg-4 p-0">
                <?php $this->load->view('components/sections/insuranceSpecial'); ?>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-8 pr-5 pl-0 py-5 mobile_correction_left billing" id="billing_policy">
            <p class="font26 b600 purple">Billing Policy</p>
            <p class="font18 mb-2">Our billing policy has recently changed. Please review prior to your appointment.</p>
            <?php $this->load->view('components/sections/accordion'); ?>                  
        </div>
    </div>
</div>

<?php $this->load->view('components/common/footer'); ?>