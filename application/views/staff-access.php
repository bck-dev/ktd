<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>


    <div class="container staffaccess-content-area">
        <div class="jumbotron staffaccess-jumbotron">
            <h3 class="staffaccess-heading">Join our KTMG staff portal and have all your work organized in one space</h3>
            <!-- <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="row">
                <div class="col-md-5">
                    <p class="staffaccess-text">Already have an account ?</p>
                    <a href="https://login.microsoftonline.com/common/oauth2/authorize?client_id=4345a7b9-9a63-4910-a426-35363201d503&redirect_uri=https%3A%2F%2Fwww.office.com%2Flanding&response_type=code%20id_token&scope=openid%20profile&response_mode=form_post&nonce=637175224669892996.YmY2NDBhMzktYjUxNC00ZDkwLTlkNjgtM2Y4YmRjNGQyZDI1ZGM3NjI0OTAtMzE0NC00OTM1LWE2OWUtNzcyZWE0M2U1YzMx&ui_locales=en-US&mkt=en-US&client-request-id=1126c4cc-4135-4834-bddd-15ef21e48c93&state=vnpDxQn0wfMSMRP7TqJuKUMqVnr3dZR-mxVDmU1h6LVLQorzMKVtmx5Ct-qubs0vQ7vE4HCZrBLLhViuX-Sksrq5z3a3D0Ns8MLUC6MGxdW0dg5pApqPkpQxaMlBtBUgimK_3WIFrJnAfNPXlCBVw8w4_EhOT7iGgYx8t_WJWRl_-5QcDIkl_eKoI9eMSh-RYTp3JzdjqQMxD9OYwiqPEXw8bk07EjfvLmsQvtiIYWNimBcrhoDRUCeBekXQk78Xicn1CfVJrEa8zSTk2u24T2KWxPA4ApnuPfX-uVXYczIqGvZbmJDCHBuEjeayHejx1dBLXqBeqKD1i5Nqu3tuUTZTgqnU2y8IRxAGMYcR3MJELkdiVUCZNYw9ZcDiTHDKWEpwWvCnvREhSxi9lmj3YCBTF-0Cc1VjPmpWa4yN0em7KXpooxcV67_6ssEpkOfTTfNGkpNGHS5gMxjxwMuuGEC7NdALV_zonxRwA_4Pj8u_EQ5jMZbUwkSGzS_V9Fr_V_8bQNK9LSCEqFL92d94JikRmUs5Bv8s1saNEjYu-SgOJ_qO_VRbgqXjdB91smsDJGH-7YX-JA-Pz8nqzoVkdebgKacXG3tZvElNFnQc-1N28kyjNem35ErDbqtCKsSmci7gQ2rSBD9wPQTuz0tj7A&x-client-SKU=ID_NETSTANDARD2_0&x-client-ver=6.3.0.0&sso_reload=true" target="blank"><button type="button" class="btn btn-primary staffaccess-login">Log In</button></a>
                </div>
                <div class="col-md-1">
                    <div class="staffaccess-vl"></div>
                </div>
                <div class="col-md-5">
                    <p class="staffaccess-text">Create a new account</p>
                    <a href="https://login.microsoftonline.com/common/oauth2/authorize?client_id=4345a7b9-9a63-4910-a426-35363201d503&redirect_uri=https%3A%2F%2Fwww.office.com%2Flanding&response_type=code%20id_token&scope=openid%20profile&response_mode=form_post&nonce=637175224669892996.YmY2NDBhMzktYjUxNC00ZDkwLTlkNjgtM2Y4YmRjNGQyZDI1ZGM3NjI0OTAtMzE0NC00OTM1LWE2OWUtNzcyZWE0M2U1YzMx&ui_locales=en-US&mkt=en-US&client-request-id=1126c4cc-4135-4834-bddd-15ef21e48c93&state=vnpDxQn0wfMSMRP7TqJuKUMqVnr3dZR-mxVDmU1h6LVLQorzMKVtmx5Ct-qubs0vQ7vE4HCZrBLLhViuX-Sksrq5z3a3D0Ns8MLUC6MGxdW0dg5pApqPkpQxaMlBtBUgimK_3WIFrJnAfNPXlCBVw8w4_EhOT7iGgYx8t_WJWRl_-5QcDIkl_eKoI9eMSh-RYTp3JzdjqQMxD9OYwiqPEXw8bk07EjfvLmsQvtiIYWNimBcrhoDRUCeBekXQk78Xicn1CfVJrEa8zSTk2u24T2KWxPA4ApnuPfX-uVXYczIqGvZbmJDCHBuEjeayHejx1dBLXqBeqKD1i5Nqu3tuUTZTgqnU2y8IRxAGMYcR3MJELkdiVUCZNYw9ZcDiTHDKWEpwWvCnvREhSxi9lmj3YCBTF-0Cc1VjPmpWa4yN0em7KXpooxcV67_6ssEpkOfTTfNGkpNGHS5gMxjxwMuuGEC7NdALV_zonxRwA_4Pj8u_EQ5jMZbUwkSGzS_V9Fr_V_8bQNK9LSCEqFL92d94JikRmUs5Bv8s1saNEjYu-SgOJ_qO_VRbgqXjdB91smsDJGH-7YX-JA-Pz8nqzoVkdebgKacXG3tZvElNFnQc-1N28kyjNem35ErDbqtCKsSmci7gQ2rSBD9wPQTuz0tj7A&x-client-SKU=ID_NETSTANDARD2_0&x-client-ver=6.3.0.0&sso_reload=true" target="blank"><button type="button" class="btn btn-success staffaccess-signup">Sign Up</button></a>
                </div>
            </div>
                </div>
                <div class="col-md-3"></div>
            </div> -->

            <div class="row">
                <div align="center" class="col">
                    <!-- <p class="staffaccess-text">Already have an account ?</p> -->
                    <a href="https://login.microsoftonline.com/common/oauth2/authorize?client_id=4345a7b9-9a63-4910-a426-35363201d503&redirect_uri=https%3A%2F%2Fwww.office.com%2Flanding&response_type=code%20id_token&scope=openid%20profile&response_mode=form_post&nonce=637175224669892996.YmY2NDBhMzktYjUxNC00ZDkwLTlkNjgtM2Y4YmRjNGQyZDI1ZGM3NjI0OTAtMzE0NC00OTM1LWE2OWUtNzcyZWE0M2U1YzMx&ui_locales=en-US&mkt=en-US&client-request-id=1126c4cc-4135-4834-bddd-15ef21e48c93&state=vnpDxQn0wfMSMRP7TqJuKUMqVnr3dZR-mxVDmU1h6LVLQorzMKVtmx5Ct-qubs0vQ7vE4HCZrBLLhViuX-Sksrq5z3a3D0Ns8MLUC6MGxdW0dg5pApqPkpQxaMlBtBUgimK_3WIFrJnAfNPXlCBVw8w4_EhOT7iGgYx8t_WJWRl_-5QcDIkl_eKoI9eMSh-RYTp3JzdjqQMxD9OYwiqPEXw8bk07EjfvLmsQvtiIYWNimBcrhoDRUCeBekXQk78Xicn1CfVJrEa8zSTk2u24T2KWxPA4ApnuPfX-uVXYczIqGvZbmJDCHBuEjeayHejx1dBLXqBeqKD1i5Nqu3tuUTZTgqnU2y8IRxAGMYcR3MJELkdiVUCZNYw9ZcDiTHDKWEpwWvCnvREhSxi9lmj3YCBTF-0Cc1VjPmpWa4yN0em7KXpooxcV67_6ssEpkOfTTfNGkpNGHS5gMxjxwMuuGEC7NdALV_zonxRwA_4Pj8u_EQ5jMZbUwkSGzS_V9Fr_V_8bQNK9LSCEqFL92d94JikRmUs5Bv8s1saNEjYu-SgOJ_qO_VRbgqXjdB91smsDJGH-7YX-JA-Pz8nqzoVkdebgKacXG3tZvElNFnQc-1N28kyjNem35ErDbqtCKsSmci7gQ2rSBD9wPQTuz0tj7A&x-client-SKU=ID_NETSTANDARD2_0&x-client-ver=6.3.0.0&sso_reload=true" target="blank"><button type="button" class="btn btn-primary staffaccess-login">Log In</button></a>
                </div>
            </div>
            
        </div>
    </div>
            

<!-- <?php $this->load->view('components/sections/appointmentForm'); ?>  -->
<?php $this->load->view('components/common/footer'); ?>