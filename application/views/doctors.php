<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<?php $this->load->view('components/sections/banner'); ?>

<div class="container pt-5">
  <div class="row">
    <div class="col-lg-8">
      <div class="w-100 light_blue_background p-3 d-block d-lg-none">
        <form action="<?php echo base_url('sortdoctors');?>" method="GET" name="sortForm" >
          <p class="font26 white">Filter by location</p>
          <select class="custom-select" name='location' style="width: 100%" required>
            <?php if($selectedLoc->name): ?>
              <option value="<?php echo $selectedLoc->id;?>" selected="selected"><?php echo $selectedLoc->name;?></option>
              <option disabled="disabled" style="font-size: 5px;"> -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</option>
              <option value="ALL">All Locations</option>
            <?php else:?>
              <option value="ALL" selected="selected">All Locations</option>
            <?php endif; ?>
            <?php foreach($locationData as $location): ?>
              <option value="<?php echo $location->name;?>"><?php echo $location->name; ?></option>
            <?php endforeach; ?>
          </select></br></br>  
          <button type="submit" class="btn btn-block btn-custom b700" >FILTER</button>
        </form>
      </div> 
      <?php foreach($doctorData as $dataRow): ?>
        <div class="row no-gutters"> 
          <div class="col-md-4 text-center" >
              <img src="<?php echo base_url(); ?>/uploads/<?php echo $dataRow->image; ?>" class="card-img" alt="" />
              <p class="font20 b600 dark_grey m-0 mt-3"><?php echo $dataRow->name; ?></p>
              <p class="font15 b300 dark_grey m-0"><?php echo $dataRow->certification; ?></p> 
              <p class="font15 b300 dark_grey m-0"><?php echo $dataRow->qualification; ?></p> 
              <p class="font12 b600 dark_grey m-0 mt-3">SPECIALTY</p>
              <p class="font15 b300 dark_grey"><?php echo $dataRow->specialty; ?></p>  

              <form id="makeAppointment<?php echo $dataRow->id; ?>" action="<?php echo base_url('doctors/appointment') ?>" method="POST">
                <div id="yellow-select">
                  <select class="yellow-select b700" id="makeAppointmentLocation<?php echo $dataRow->id; ?>" name='locationId' required>
                    <option class="p-5" selected="selected" disabled="disabled">MAKE AN APPOINTMENT</option>
                    <?php foreach($dataRow->doclocations as $location): ?>
                      <option value="<?php echo $location->id; ?>" ><?php echo $location->name; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>                 
                
              </form>
          </div>
          <div class="col-md-8 p-4 pt-2">
            <p class="font12 b700 dark_grey">LOCATIONS</p>
            <?php foreach($dataRow->doclocations as $location): ?>                    
              <p class="font20 b700 light_blue m-0">
                <?php echo $location->address; ?> 
              </p>

              <hr/>
              <div class="d-flex">
                <div class="w-50">
                  <p class="font12">
                    <span class="b600 green">
                      <?php 
                        if($location->status == 'Close'): 
                          echo "<span style='color:red;'>(Closed Today)</span>"; 
                        else:                     
                          ini_set('date.timezone', 'America/Los_Angeles');
                          $time = strtotime(date('H:i'));
                          $toTime = strtotime($location->toHour);
                          $fromTime = strtotime($location->fromHour);
                        ?> 
                                                                     
                        <?php if($time>$fromTime && $time<$toTime): ?>
                          Hrs <b><?php echo date("g:i a", strtotime($location->fromHour))."-".date("g:i a", strtotime($location->toHour)); ?> (Open Now)</b>  
                        <?php
                          else:
                            echo "<span style='color:red;'>Hrs <b>". date("g:i a", strtotime($location->fromHour))."-".date("g:i a", strtotime($location->toHour))." (Closed Now)</b></span>";
                          endif;
                        endif; 
                        ?>
                    </span>
                  </p>
                </div>
                <div class="w-50 text-right">
                  <a class="font12 green" href="https://maps.google.com/?q=<?php echo $location->latitude; ?>,<?php echo $location->longitude; ?>" target="_blank">Get Directions</a>
                </div>
              </div>
                               
            <?php endforeach; ?>

            <div class="d-flex mt-4">
                <div class="w-50">
                  <p class="font15">
                    Tel. <a class="font15 dark_grey b600" href="tel:8183615437">(818) 361-5437</a><br />
                  </p>
                </div>
                <div class="w-50 text-right dark_purple">
                  Text Us<br />
                  English <a class="doc-contact-right dark_purple b600" href="sms:6262987121">(626) 298-7121</a><br />
                  Spanish <a class="doc-contact-right dark_purple b600" href="sms:6262697744">(626) 269-7744</a><br />
                </div>
              </div>

          </div>
        </div>
        <hr class="mb-5"/>
      <?php endforeach; ?>
    </div>

 
  <div class="col-lg-4">
    <div class="sticky">
      <div class="w-100 light_blue_background p-3 d-none d-lg-block">
        <form action="<?php echo base_url('sortdoctors');?>" method="GET" name="sortForm" >
          <p class="font26 white">Filter by location</p>
          <select class="custom-select" name='location' style="width: 100%" required>
            <?php if($selectedLoc->name): ?>
              <option value="<?php echo $selectedLoc->id;?>" selected="selected"><?php echo $selectedLoc->name;?></option>
              <option disabled="disabled" style="font-size: 5px;"> -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</option>
              <option value="ALL">All Locations</option>
            <?php else:?>
              <option value="ALL" selected="selected">All Locations</option>
            <?php endif; ?>
            <?php foreach($locationData as $location): ?>
              <option value="<?php echo $location->name;?>"><?php echo $location->name; ?></option>
            <?php endforeach; ?>
          </select></br></br> 
          <button type="submit" class="btn btn-block btn-custom b700" >FILTER</button>
        </form>
      </div> 
      <div  class="purple_background p-0 pb-3 featured_box m-0">
        <div class="w-100">
          <div class="p-5">
            <div class="white" style="font-size: 22px;">
              Our board certified pediatricians are dedicated to providing your child with the highest degree of health care.
            </div>
          </div>
            <div class="w-100 white_background p-4 text-center" style="position: relative; bottom: 0;">
              <a href="<?php echo base_url('locations');?>" class="purple font18 b600">Our Locations</a>
            </div>
        </div>
      </div>
    </div>
    
  </div>
</div>
</div>

<script>
  $(document).ready(function(){
    <?php foreach($doctorData as $dataRow): ?>
      $('#makeAppointmentLocation<?php echo $dataRow->id; ?>').change(function(){
        $('#makeAppointment<?php echo $dataRow->id; ?>').submit();
      });
    <?php endforeach; ?>
  });
</script>

<?php $this->load->view('components/common/footer'); ?>