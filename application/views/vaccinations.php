<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<?php $this->load->view('components/sections/banner'); ?>
<?php $this->load->view('components/sections/descriptionBox'); ?>

<div class="container d-sm-block d-lg-none d-flex justify-content-center">
  <div class="row">
    <div class="col-sm">
    <a class="downloadable btn btn-info " href="<?php echo base_url('assets/files/0-18yrs-child-combined-schedule.pdf');?>" target="_blank">
       Vaccine and Immunization Schedule
    </a>
    </div>
  </div>
</div>

<div class="container d-none d-lg-block">
  <div class="row">
    <div class="col-sm pl-0">
    <a class="downloadable btn btn-info " href="<?php echo base_url('assets/files/0-18yrs-child-combined-schedule.pdf');?>" target="_blank">
       Vaccine and Immunization Schedule
    </a>
    </div>
  </div>
</div>
<br><br>

<?php $this->load->view('components/sections/specialities'); ?>
<?php $this->load->view('components/common/footer'); ?>
