<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<?php $this->load->view('components/sections/banner'); ?>
<?php $this->load->view('components/sections/descriptionBox'); ?>
<?php $this->load->view('components/sections/specialities'); ?>
<?php $this->load->view('components/sections/textBox'); ?>
<?php $this->load->view('components/common/footer'); ?>