<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<?php $this->load->view('components/sections/bannerv3'); ?>

<div class="container">
    <div class="row dark_grey">
        <div class="col-lg-8 pr-5 pl-0 py-5 mobile_correction_left">
            <p class="font26 b600">Contact Kids & Teens Medical Group</p>
            <p class="font20 mt-3">General Contact Information</p>
            <p class="font16 b600">504 S. Sierra Madre Blvd <br />
                Pasadena, CA 91107<br /><br />
                <a href="tel:8183615437" class="font16 b600 dark_grey">(818) 361-5437</a><br />
                <a href="mailto:info@ktdoctor.com" class="font16 b600 dark_grey">info@ktdoctor.com</a>
            </p>
            <p class="font20 mt-5">Other Information</p>
            <div class="row py-3" style="border-top: 1px solid #cccccc;">
                <div class="col-lg-4">Insurance and billing</div>
                <div class="col-lg-4"><a href="mailto:insurance@ktdoctor.com" class="font16 dark_grey">insurance@ktdoctor.com</a></div>
                <div class="col-lg-4 text-right"><a class="font15 dark_grey" href="tel:8183615437">(818) 361-5437</a></div>
            </div>
            <div class="row py-3" style="border-top: 1px solid #cccccc;">
                <div class="col-lg-4">Patient Feedback</div>
                <div class="col-lg-4"><a href="mailto:feedback@ktdoctor.com" class="font16 dark_grey">feedback@ktdoctor.com</a></div>
                <div class="col-lg-4 text-right"><a class="font15 dark_grey" href="tel:8183615437">(818) 361-5437</a></div>
            </div>

            <p class="font20 mt-5">Online Information</p>
            <div class="row py-3" style="border-top: 1px solid #cccccc;">
                <div class="col-lg-4">Youtube</div>
                <div class="col-lg-4" style="overflow-wrap: break-word;">youtube.com/channel/UC5pMXGZ_F2OZUFdfy6YbIew</div>
                <div class="col-lg-4 text-right">
                    <a href="https://www.youtube.com/channel/UC5pMXGZ_F2OZUFdfy6YbIew" class="light_blue" target="blank">Link</a>
                </div>
            </div>            
            <div class="row py-3" style="border-top: 1px solid #cccccc;">
                <div class="col-lg-4">Facebook</div>
                <div class="col-lg-4">facebook.com/kidsandteensmedicalgroup/</div>
                <div class="col-lg-4 text-right">
                    <a href="http://facebook.com/kidsandteensmedicalgroup/" class="light_blue" target="blank">Link</a>
                </div>
            </div>
            <div class="row py-3" style="border-top: 1px solid #cccccc;">
                <div class="col-lg-4">Instagram</div>
                <div class="col-lg-4">instagram.com/ktmedicalgroup</div>
                <div class="col-lg-4 text-right">
                    <a href="https://www.instagram.com/ktmedicalgroup/" class="light_blue" target="blank">Link</a>
                </div>
            </div>

        </div>
        <div class="col-lg-4 p-0 py-7 contact_responsive">
            <?php $this->load->view('components/sections/textUs'); ?>
        </div>
    </div>
</div>

<?php $this->load->view('components/common/footer'); ?>