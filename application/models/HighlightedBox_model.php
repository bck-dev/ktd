<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class HighlightedBox_model extends CI_Model
{
    
    public function highlightedBoxFull() {
        $this->db->select('highlighted_box.*, pages.name');
        $this->db->from('highlighted_box');
        $this->db->join('pages', "highlighted_box.pageId = pages.id");
        return $this->db->get()->result();
    }

    public function getPage($id) {
        $this->db->select('highlighted_box.*, pages.name');
        $this->db->from('highlighted_box');
        $this->db->where('highlighted_box.id', $id);
        $this->db->join('pages', "highlighted_box.pageId = pages.id");
        return $this->db->get()->result();
    }

}