<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Insurance_model extends CI_Model
{
    
    public function insuranceFull() {
        $this->db->select('location_has_insurance.*, insurance.insuranceName,locations.name');
        $this->db->from('location_has_insurance');
        $this->db->join('insurance', "location_has_insurance.insuranceId = insurance.id");
        $this->db->join('locations', "location_has_insurance.locationId = locations.id");
        return $this->db->get()->result();
    }

    public function locationFull() {
        $this->db->select('location_has_insurance.*, locations.name');
        $this->db->from('location_has_insurance');
        $this->db->join('locations', "location_has_insurance.locationId = locations.id");
        return $this->db->get()->result();
    }

}