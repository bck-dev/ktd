<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class LogoSlider_model extends CI_Model
{
    
    public function logoSliderFull() {
        $this->db->select('logo_slider.*, pages.name');
        $this->db->from('logo_slider');
        $this->db->join('pages', "logo_slider.pageId = pages.id");
        return $this->db->get()->result();
    }

}