<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['home'] = 'home';
$route['about'] = 'home/about';
$route['newborncare'] = 'home/newBornCare';
$route['newborn'] = 'home/newBornCare';
$route['expectantmothers'] = 'home/expectantMothers';
$route['practice'] = 'home/practiceOpportunities';
$route['vaccinations'] = 'home/vaccinations';
$route['locations'] = 'home/locations';
$route['locations/(:any)'] = 'home/locations/$1';
$route['insurance'] = 'home/insurance';
$route['appointment'] = 'home/appointment';
$route['appointmentnew'] = 'home/appointmentnew';
$route['appointment/(:any)/(:num)'] = 'home/appointment/$1/$2';
$route['doctors/appointment'] = 'home/doctorsAppointment';
$route['doctors'] = 'home/doctors';
$route['sortdoctors'] = 'home/sortDoctors';
$route['reviews'] = 'home/reviews';
$route['faq'] = 'home/faq';
$route['test'] = 'home/test';
$route['appointment/book'] = 'home/book';
$route['career'] = 'home/career';
$route['affiliated'] = 'home/affiliated';
$route['existing'] = 'home/existing';
$route['contact'] = 'home/contact';
$route['afterhour'] = 'home/afterhour';
$route['mailredirect'] = 'home/mailredirect';
$route['search'] = 'home/search';
$route['telehealth'] = 'home/telehealth';
$route['blog'] = 'home/blog';
$route['test'] = 'home/test';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login'] = 'admin/login';
$route['admin'] = 'admin/login';
$route['login/check'] = 'admin/login/checkLogin';
$route['logout'] = 'admin/user/logout';

$route['admin/location'] = 'admin/location';
$route['admin/location/new'] = 'admin/location/new';
$route['admin/location/save'] = 'admin/location/save';
$route['admin/location/update'] = 'admin/api/update';
$route['admin/location/delete/(:any)'] = 'admin/location/delete/$1';

$route['admin/doctor'] = 'admin/location/doctor';

$route['admin/users'] = 'admin/user/addUser';
$route['admin/user/delete/(:any)'] = 'admin/user/delete/$1';
$route['admin/user/loadupdate/(:any)'] = 'admin/user/loadUpdate/$1';
$route['admin/user/update/(:any)'] = 'admin/user/update/$1';

$route['admin/accordion'] = 'admin/accordion';
$route['admin/accordion/add'] = 'admin/accordion/add';
$route['admin/accordion/delete/(:any)'] = 'admin/accordion/delete/$1';
$route['admin/accordion/update/(:any)'] = 'admin/accordion/update/$1';
$route['admin/accordion/loadupdate/(:any)'] = 'admin/accordion/loadUpdate/$1';

$route['admin/featurebox'] = 'admin/featureBox';
$route['admin/featurebox/add'] = 'admin/featureBox/add';
$route['admin/featurebox/delete/(:any)'] = 'admin/featureBox/delete/$1';
$route['admin/featurebox/update/(:any)'] = 'admin/featureBox/update/$1';
$route['admin/featurebox/loadupdate/(:any)'] = 'admin/featureBox/loadUpdate/$1';

$route['admin/banner'] = 'admin/banner';
$route['admin/banner/add'] = 'admin/banner/add';
$route['admin/banner/delete/(:any)'] = 'admin/banner/delete/$1';
$route['admin/banner/update/(:any)'] = 'admin/banner/update/$1';
$route['admin/banner/loadupdate/(:any)'] = 'admin/banner/loadUpdate/$1';

$route['admin/descriptionbox'] = 'admin/descriptionBox';
$route['admin/descriptionbox/add'] = 'admin/descriptionBox/add';
$route['admin/descriptionbox/delete/(:any)'] = 'admin/descriptionBox/delete/$1';
$route['admin/descriptionbox/update/(:any)'] = 'admin/descriptionBox/update/$1';
$route['admin/descriptionbox/loadupdate/(:any)'] = 'admin/descriptionBox/loadUpdate/$1';

$route['admin/logoslider'] = 'admin/logoSlider';
$route['admin/logoslider/add'] = 'admin/logoSlider/add';
$route['admin/logoslider/delete/(:any)'] = 'admin/logoSlider/delete/$1';
$route['admin/logoslider/update/(:any)'] = 'admin/logoSlider/update/$1';
$route['admin/logoslider/loadupdate/(:any)'] = 'admin/logoSlider/loadUpdate/$1';

$route['admin/textbox'] = 'admin/textBox';
$route['admin/textbox/add'] = 'admin/textBox/add';
$route['admin/textbox/delete/(:any)'] = 'admin/textBox/delete/$1';
$route['admin/textbox/update/(:any)'] = 'admin/textBox/update/$1';
$route['admin/textbox/loadupdate/(:any)'] = 'admin/textBox/loadUpdate/$1';

$route['admin/highlightedbox'] = 'admin/highlightedBox';
$route['admin/highlightedbox/add'] = 'admin/highlightedBox/add';
$route['admin/highlightedbox/delete/(:any)'] = 'admin/highlightedBox/delete/$1';
$route['admin/highlightedbox/update/(:any)'] = 'admin/highlightedBox/update/$1';
$route['admin/highlightedbox/loadupdate/(:any)'] = 'admin/highlightedBox/loadUpdate/$1';

$route['admin/insurance/delete/(:any)'] = 'admin/insurance/delete/$1';
$route['admin/insurance/update/(:any)'] = 'admin/insurance/update/$1';

$route['admin/doctor'] = 'admin/doctor';
$route['admin/doctor/newdoctor'] = 'admin/doctor/newDoctor';
$route['admin/doctor/add'] = 'admin/doctor/add';
$route['admin/doctor/delete/(:any)'] = 'admin/doctor/delete/$1';
$route['admin/doctor/update/(:any)'] = 'admin/doctor/update/$1';
$route['admin/doctor/loadupdate/(:any)'] = 'admin/doctor/loadUpdate/$1';

$route['admin/pages'] = 'admin/pages';
$route['admin/pages/add'] = 'admin/pages/add';
$route['admin/pages/delete/(:any)'] = 'admin/pages/delete/$1';
$route['admin/pages/update/(:any)'] = 'admin/pages/update/$1';
$route['admin/pages/loadupdate/(:any)'] = 'admin/pages/loadUpdate/$1';

$route['admin/reviews'] = 'admin/reviews';
$route['admin/reviews/add'] = 'admin/reviews/add';
$route['admin/reviews/delete/(:any)'] = 'admin/reviews/delete/$1';
$route['admin/reviews/update/(:any)'] = 'admin/reviews/update/$1';
$route['admin/reviews/loadupdate/(:any)'] = 'admin/reviews/loadUpdate/$1';

//api
$route['api/doctor'] = 'admin/doctor/api';
$route['api/highlightedbox'] = 'admin/highlightedBox/api';
$route['api/textbox'] = 'admin/textBox/api';
$route['api/descriptionbox'] = 'admin/descriptionBox/api';
$route['api/banner'] = 'admin/banner/api';
$route['api/featurebox'] = 'admin/featureBox/api';
$route['api/accordion'] = 'admin/accordion/api';
$route['api/review'] = 'admin/reviews/api';


$route['web/api/location'] = 'home/lapi';
$route['web/api/homedoctor'] = 'home/homeDoctorApi';
$route['web/api/telehealth'] = 'home/teleapi';