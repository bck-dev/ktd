$(document).ready(function(){
    $('.search_box').keyup(function(){
        $('.search_box_img').hide(200);
    }).focusout(function(){
        $('.search_box_img').show(200);
    });

    $('.blog_close').click(function(){
        $('.blog_bar').addClass('d-none');
    });

    // menu
    $(window).scroll(function () {

        if ($(document).scrollTop() > 10) {

            $('.menu').addClass('menu_scroll');
            $('.top_bar').css("display", "none");
            $('#appointmentMenu').removeClass('d-none');
            $('#appointmentMenu').addClass('d-block');
            $('#appointmentMenuMobile').removeClass('d-none');
            $('#appointmentMenuMobile').addClass('d-block');

        } else {
            
            $('.menu').removeClass('menu_scroll');
            $('.top_bar').css("display", "block");            
            $('#appointmentMenu').removeClass('d-block');
            $('#appointmentMenu').addClass('d-none');
            $('#appointmentMenuMobile').removeClass('d-block');
            $('#appointmentMenuMobile').addClass('d-none');

        }

    });

    // hospital logo slider start

    $('.hospital-logos').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4500,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 3
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 2
            }
        }]
    });

    $('.news_slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 8000,
        arrows: false,
        dots: false,
        pauseOnHover: false
    });    
});

